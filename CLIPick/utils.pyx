# COPYRIGHT (C) 2016 By Park, Sihyung
# cythonized utility for ISRIP

import random
import numpy as np
import pandas as pd
from collections import defaultdict

class MicroArray(pd.DataFrame):
    def initialize_columns(self):
        self.columns = ["RefSeq", "length", "expression"]
        return


class RNASeq(pd.DataFrame):
    def initialize_columns(self):
        self.columns = ["RefSeq", "length", "expression"]
        return


class IPTags(pd.DataFrame):
    def initialize_columns(self):
        self.columns = ["RefSeq", "peak name", "peak_height"]
        return


class Processed(pd.DataFrame):
    @staticmethod
    def load(file_path, header=None):
        self = Processed(pd.read_csv(file_path, sep="\t", header=header))
        self.initialize_columns()
        return self

    def initialize_columns(self):
        self.columns = ["RefSeq", "length", "expression", "exp_tag"]
        return


class SmoothBedGraph(pd.DataFrame):
    @staticmethod
    def load(file_path, header=None):
        self = SmoothBedGraph(pd.read_csv(file_path, sep="\t", header=header))
        self.initialize_columns()
        return self

    def initialize_columns(self):
        self.columns = ["chrom", "chrstart", "chrend", "overlap", "strand"]


class CalledPeak(pd.DataFrame):
    @staticmethod
    def load(file_path, header=None):
        self = CalledPeak(pd.read_csv(file_path, sep="\t", header=header))
        self.initialize_columns()
        return self

    def initialize_columns(self):
        self.columns = ["RefSeq", "peak", "peak_height", "chrom", "peak_pos", "strand"]


class ISRIPTags(pd.DataFrame):
    @staticmethod
    def load(file_path, header=None):
        self = ISRIPTags(pd.read_csv(file_path, sep="\t", header=header))
        self.initialize_columns()
        return self

    def initialize_columns(self):
        self.columns = ["RefSeq", "length", "expression", "exp_tag", "threshold", "thres_cum_prob"]


class ReportedPeak(pd.DataFrame):
    @staticmethod
    def load(file_path, header=None):
        self = ReportedPeak(pd.read_csv(file_path, sep="\t", header=header))
        self.initialize_columns()
        return self

    def initialize_columns(self):
        self.columns = ["RefSeq", "length", "expression", "exp_tag", "threshold",
                        "thres_cum_prob", "peak", "peak_height", "chrom", "peak_pos", "strand", "FDR_<="]

    def filter_out(self, min_fdr=0.05, inplace=False):
        if inplace:
            self = ReportedPeak(self[self["FDR_<="]<min_fdr])
            return
        return ReportedPeak(self[self["FDR_<="]<min_fdr])


def generate_random_fragments(length, exp_tag, lower, upper, frag_avg_size=50):
    """
     * Input: randomly generate fragments of size (lower < size < upper).
     * Output: a tuple which consists of a dict and a number.
               ({n: (start-site, end-site), ...}, total)
    """
    total = 0
    res_list = []
    while total < exp_tag:
        frag_size = int(round(random.gauss(frag_avg_size, frag_avg_size//5.)))
        while frag_size <= 0:
            frag_size = int(round(random.gauss(frag_avg_size, frag_avg_size//5.)))
        if frag_size >= length:
            #central = random.randint(1 + int(round(frag_size//2.)), length)
            res_list.append((1, length, length))
        else:
            central = random.randint(1 + int(round(frag_size//2.)),
                                     length - (frag_size//2))
            st = central - int(round(frag_size//2.))
            en = central + (frag_size//2)
            if lower < frag_size < upper:
                res_list.append((st, en, frag_size))
                total += 1

    return tuple(res_list)


def simulate_sequencing(random_fragments, read_type, read_length):
    """
     * Input: [1]result from random_fragment()
              [2]sequencing read type, [3]sequencing read length
               of simulating system
     * Output: a dict of every fragments.
               {(read-start-site, read-end-site): fragment_count, ..}
     Selects fragments as many as expected tag number('tags'), simulates
     sequencing reads (e.g. Illumina reads only 35 nt), and returns
     dictionary of read sites
    """
    f_ans = defaultdict(int)
    if read_type == "s":
        for st, en, frag_size in random_fragments:
            if frag_size > read_length:
                f_ans[(st, st + read_length)] += 1
            else:
                f_ans[(st, en)] += 1
    elif read_type == "p":
        for st, en, frag_size in random_fragments:
            f_ans[(st, en)] += 1
    else:
        raise ValueError("not a valid read_type: `{}`".format(read_type))

    #if isinstance(dict(f_ans), float): print(random_fragments); print("") #########

    return dict(f_ans)
    # f_ans = {(read-start-site, read-end-site): fragment_count, ..}


def overlap(f_ans):
    """
     * Input: result from simulation()
              {(read-start-site, read-end-site): fragment_count, ..}
     * Output: maximum number of fragment overlaps
    """
    #f_ans = {(1, 5):1, (1, 7):3}
    f_ans_d = {}
    for st, en in f_ans.keys():
        f_ans_d[st] = en
    ens = list(f_ans_d.values())
    template = np.zeros(max(ens), dtype=int)    # = array([0, 0, 0, 0, 0, ..])
    for st, en in f_ans.keys():
        template[st:en+1] += f_ans[(st, en)]

    if template.size == 0:
        return 0
    return int(np.max(template))   ### maximum number of fragment overlaps


def get_threshold(length, exp_tag, n_iter, lower, upper,
                  read_type, read_length):
    """
    * Returns max overlap number and 2nd max overlap number of reads
    """
    overlap_iterdata = []
    for i in range(n_iter):
        random_frags = generate_random_fragments(length, exp_tag, lower, upper)
        f_ans = simulate_sequencing(random_frags, read_type, read_length)
        max_overlap = overlap(f_ans)
        overlap_iterdata.append(max_overlap)
    overlap_res = list(sorted(list(set(overlap_iterdata)), reverse=True))  # drop redundancy

    return [(overlap_res[0]+1, 0)]+[(overlap_max, overlap_iterdata.count(overlap_max))\
            for overlap_max in overlap_res]+[(0, 0)]


def get_threshold_apply(df, n_iter, lower, upper, read_type, read_length, frag_avg_size=50):
    """
    df.apply version of `get_threshold()`
    * Returns overlap number of reads
    """
    df.dropna(inplace=True)
    random_frags = df.apply(lambda row: generate_random_fragments(row["length"],
                    row["exp_tag"], lower, upper, frag_avg_size), axis=1)
    try:
        f_ans = random_frags.apply(lambda frag_list: simulate_sequencing(list(frag_list), read_type, read_length))
        max_overlap = f_ans.apply(lambda simseq: [overlap(simseq)])
        overlap_iterdata = max_overlap
        for i in range(n_iter-1):
            random_frags = df.apply(lambda row: generate_random_fragments(row["length"],
                            row["exp_tag"], lower, upper, frag_avg_size), axis=1)
            f_ans = random_frags.apply(lambda frag_list: simulate_sequencing(list(frag_list), read_type, read_length))
            max_overlap = f_ans.apply(lambda simseq: [overlap(simseq)])
            overlap_iterdata += max_overlap
        overlap_res = overlap_iterdata.apply(lambda row: sorted(list(set(row)), reverse=True))
        ov = pd.DataFrame(np.array([overlap_iterdata, overlap_res]).T, columns=["ov_iterdata", "ov_res"])

        def return_ov(row):
            ov_data, ov_res = row
            _1 = [(ov_res[0]+1, 0)]
            _2 = [(ov_max, ov_data.count(ov_max)) for ov_max in ov_res]
            _3 = [(0, 0)]
            return _1 + _2 + _3

        return ov.apply(return_ov, axis=1)
    except AttributeError:
        return np.nan


class NoClusterError(Exception):
    pass
