# COPYRIGHT (C) 2016 By Park, Sihyung
# module for IP-seq peak calling

import os, sys
import pandas as pd
import numpy as np
import sqlite3 as db
from scipy.interpolate import CubicSpline, interp1d, splrep, splev
from scipy.ndimage.filters import gaussian_filter
from .datasets import load_genome
from .__init__ import get_exome_data, get_genome_file
from .utils import SmoothBedGraph, CalledPeak

class BedGraphConverter(object):
    """
    Class for BED-to-smoothed BedGraph conversion.
    """
    def __init__(self):
        self.bed = None
        self.assembly = None
        self.total_reads = None

    def fit(self, file_path, genome_assembly, insert=50,   #read_length, avg_frag_size,
            strand_index=5, drop_st_en=True):
        """
         - file_path: path to BED file.
         - strand_index: location of strand info at input BED. (default=5)
         - genome_assembly: genome assembly to annotate BED file. ex. "hg19",
              or DataFrame object of loaded genome/exome BED file.
        """
        pd.options.mode.chained_assignment = None

        self.bed = file_path
        bed = pd.read_csv(self.bed, sep="\t", header=None)

        if drop_st_en:
            bed.drop_duplicates([0, 1, 2, strand_index], inplace=True)
        else:
            bed.drop_duplicates([0, 1, strand_index], inplace=True)

        self.bed = "bed_dropdup.bed"
        bed.to_csv(self.bed, sep="\t", index=False, header=None)

        ##### db-ing
        con = db.connect(get_exome_data())
        if not isinstance(genome_assembly, str):
            genome_assembly.to_sql("custom_genome", con, index=False, if_exists="replace")
            genome_assembly = "custom_genome"
        #bed.to_sql("bed_dropdup", con, index=False, if_exists="replace")
        #bed_refseq = pd.read_sql("""SELECT * FROM bed_dropdup AS b
        #                            INNER JOIN {} AS g ON b.`0` = g.chrom
        #                            AND b.`1` >= g.chrstart AND b.`2` <= g.chrend
        #                            AND b.`{}` = g.strand"""\
        #                            .format(genome_assembly, strand_index), con)
        df_exome = pd.read_sql("SELECT * FROM {}".format(genome_assembly), con)
        df_exome.to_csv("temp_exome.bed", sep="\t", index=False, header=None)
        os.system("bedtools intersect -a {} -b {} -loj > temp_loj.txt".format(self.bed, "temp_exome.bed"))
        bed_refseq = pd.read_csv("temp_loj.txt", sep="\t", header=None)
        bed_refseq = bed_refseq[bed_refseq.iloc[:, -1] != "."]
        bed_refseq["3"] = bed_refseq.iloc[:, -3].str.findall("[A-Z][A-Z]_[0-9]+").str.get(0)
        bed_refseq = bed_refseq.ix[:, :len(bed.columns)]
        bed_refseq.drop_duplicates(inplace=True)

        # stretch to match avg_frag_size
        sliced = calculate_sliced(bed_refseq, insert)
        (bed_refseq.iloc[:, 2])[bed_refseq.iloc[:, strand_index]=="+"] += sliced[bed_refseq.iloc[:, strand_index]=="+"]
        (bed_refseq.iloc[:, 1])[bed_refseq.iloc[:, strand_index]=="-"] -= sliced[bed_refseq.iloc[:, strand_index]=="-"]
        (bed_refseq.iloc[:, 1])[bed_refseq.iloc[:, 1]<0] = 0

        bed_refseq.to_csv("input_bed_refseq.bed", sep="\t", header=None, index=False)
        #con.execute("DROP TABLE bed_dropdup")
        if genome_assembly == "custom_genome":
            con.execute("DROP TABLE custom_genome")
        con.commit(); con.close(); del con
        self.bed_dropdup = "input_bed_refseq.bed"

        self.strand = strand_index
        self.total_reads = bed_refseq.shape[0]
        self.assembly = genome_assembly.lower()

        os.remove("temp_exome.bed")
        os.remove("temp_loj.txt")
        os.remove(self.bed)

    def transform(self, write_file=False):
        """
        Drop duplicates, and convert BED to 'smooth' BEDGraph using BEDTools2.
        """

        #bed = BedTool(self.bed_dropdup)
        gfile = get_genome_file(self.assembly).replace(" ", "\ ")
        os.system("bedtools sort -i {} > {}".format(self.bed_dropdup, "bed_refseq.sort.bed"))
        os.system("bedtools genomecov -strand + -bg -i {} -g {} > {}".format("bed_refseq.sort.bed", gfile, "temp_plus.bed"))
        os.system("bedtools genomecov -strand - -bg -i {} -g {} > {}".format("bed_refseq.sort.bed", gfile, "temp_minus.bed"))
        #bed.genome_coverage(bg=True, genome="mm8", strand="+").saveas("temp_plus.bed")
        #bed.genome_coverage(bg=True, genome="mm8", strand="-").saveas("temp_minus.bed")

        bedgraph_plus = pd.read_csv("temp_plus.bed", sep="\t", header=None)
        bedgraph_minus = pd.read_csv("temp_minus.bed", sep="\t", header=None)
        bedgraph_plus["strand"] = "+"
        bedgraph_minus["strand"] = "-"

        bedgraph = pd.concat([bedgraph_plus, bedgraph_minus])
        bedgraph.columns = ["chrom", "chrstart", "chrend", "overlap", "strand"]
        bedgraph.sort_values(["chrom", "strand", "chrstart"], inplace=True)

        os.remove("temp_plus.bed")
        os.remove("temp_minus.bed")
        os.remove("bed_refseq.sort.bed")
        os.remove(self.bed_dropdup)

        if write_file:
            bedgraph.to_csv("{}.bedgraph".format(self.bed), sep="\t", header=None, index=False)

        # Add spaces between each clusters.
        smooth = smoothing_bedgraph(bedgraph, 1)

        if write_file:
            fname = "{}_smooth.bedgraph".format(self.bed); n=2
            if os.path.exists(fname):       # pragma: no cover
                while os.path.exists("{}_smooth({}).bedgraph".format(self.bed, n)):
                    n += 1
                fname = "{}_smooth({}).bedgraph".format(self.bed, n)
                smooth.to_csv(fname, sep="\t", header=None, index=False)
            else:       # pragma: no cover
                smooth.to_csv(fname, sep="\t", header=None, index=False)

        return SmoothBedGraph(smooth)

    def fit_transform(self, file_path, genome_assembly, insert=50, #read_length, avg_frag_size,
                      strand_index=5, drop_st_en=True, write_file=False):
        self.fit(file_path=file_path, genome_assembly=genome_assembly,
                 insert=insert,
                 #read_length=read_length, avg_frag_size=avg_frag_size,
                 strand_index=strand_index, drop_st_en=drop_st_en)
        return self.transform(write_file=write_file)



class PeakCaller(object):

    def __init__(self, smooth_bedgraph=None, genome_assembly=None, from_file=False):
        """
        * Input
         - smooth_bedgraph: path, or DataFrame of smoothed BedGraph.
         - genome_assembly: string of reference genome assembly. ex) "hg19",
               or path to your genome/exome BED.
         - from_file: boolean of whether smooth_bedgraph is a file
        """
        self.fname = smooth_bedgraph
        self.bedgraph = smooth_bedgraph
        if from_file:
            self.fname = self.bedgraph
            self.bedgraph = SmoothBedGraph(pd.read_csv(self.bedgraph, sep="\t", header=None))
            self.bedgraph.columns = ["chrom", "chrstart", "chrend", "overlap", "strand"]
        self.assembly = genome_assembly

    def fit(self, smooth_bedgraph, genome_assembly, from_file=False):
        """
        * Input
         - smooth_bedgraph: path, or DataFrame of smoothed BedGraph.
         - genome_assembly: string of reference genome assembly. ex) "hg19",
               or path to your genome/exome BED.
         - from_file: boolean of whether smooth_bedgraph is a file
        """
        if from_file:
            self.fname = smooth_bedgraph
        else:
            self.fname = "smooth_bedgraph"
        self.bedgraph = smooth_bedgraph
        if not from_file:
            if not isinstance(self.bedgraph, SmoothBedGraph):       # pragma: no cover
                raise TypeError("inappropriate type: {}. expected {}".format(type(self.bedgraph), type(SmoothBedGraph())))
        if from_file:
            self.fname = self.bedgraph
            self.bedgraph = SmoothBedGraph(pd.read_csv(self.bedgraph, sep="\t", header=None))
            self.bedgraph.columns = ["chrom", "chrstart", "chrend", "overlap", "strand"]
        self.assembly = genome_assembly

    def call(self, verbose=True, exon_only=True, write_file=False, smoothing="weak"):
        """
        Call peak positions and their heights.

        * Input
         - write_file: boolean of whether to write output file
        """
        smooth = self.bedgraph.copy()
        called_rows = []
        xx, yy = [], []
        idx = 1
        total_r = smooth.shape[0]
        for n_r, row in smooth.iterrows():
            #if verbose:
            #    if (n_r+1)%100 == 0:
            #        prog = round((n_r+1.) / total_r * 100., 1)
            #        ###sys.stdout.flush()
            #        ###sys.stdout.write("\r * peak calling: {}%".format(prog))
            if str(row[0]).lower() == "nan":
                #if xx == [] and yy == []: continue
                called_rows += peak_call(chrom, xx, yy, idx, strand, smooth, smoothing)
                xx, yy = [], [] #xx[:0], yy[:0]
                idx += 1
            else:
                chrom = row[0]
                #xx += range(int(row[1]), int(row[2]))
                #yy += [int(row[3])] * (int(row[2])-int(row[1]))
                #xx.append((int(row[1]) + int(row[2])) // 2)
                #yy.append(int(row[3]))
                xx.append(int(row[1]))
                yy.append(int(row[3]))
                strand = row[4]

        res = pd.DataFrame([i.split("\t") for i in called_rows])
        #res = pd.read_csv(buffer, sep="\t", header=None)
        res.columns = ["chrom", "chrstart", "chrend", "peak", "peak_height", "peak_pos", "strand"]
        res.chrstart.astype(int)
        res.chrend.astype(int)
        res.peak_height.astype(float)
        res.peak_pos.astype(int)

        res = inner_join(res, self.assembly, exon_only)
        #if verbose:
        #    ###sys.stdout.flush()
        #    ###sys.stdout.write("\r * complete: 100.0%\n")
        #    pass

        if write_file:
            fname = "{}_PEAKS.bed".format(self.fname); n=2
            if os.path.exists(fname):       # pragma: no cover
                while os.path.exists("{}_PEAKS({}).bed".format(self.fname, n)):
                    n += 1
                fname = "{}_PEAKS({}).bed".format(self.fname, n)
                res.to_csv(fname, sep="\t", header=None, index=False)
            else:       # pragma: no cover
                res.to_csv(fname, sep="\t", header=None, index=False)
            ###print("\n * written as: {}".format(fname))

        return CalledPeak(res)


def calculate_sliced(bed, insert):
    if insert is not None:
        bed_len = bed.iloc[:, 2] - bed.iloc[:, 1]
        sliced = -bed_len + insert
        if np.any(sliced < 0):
            raise ValuError("'insert' should be larger than any BED sequences")
    else:
        sliced = np.array([0] * bed.shape[0])
    return sliced


def smoothing_bedgraph(bedgraph, smoothing_bases, verbose=True, return_list=False):
    """
    insert spaces between every clusters

    * Input
     - bedgraph: BedGraph to smooth
     - smoothing_bases: minimum base to differentiate each clusters
    """

    res = []
    bedgraph = bedgraph.dropna()
    bedgraph = bedgraph[bedgraph.chrom != "nan"]
    chromosomes = bedgraph.chrom.unique()
    strands = bedgraph.strand.unique()
    spacer = pd.DataFrame([["nan"]*bedgraph.shape[1]], columns=bedgraph.columns)
    for chrom in chromosomes:
        for strand in strands:
            part = bedgraph[bedgraph.chrom == chrom]
            part = part[part.strand == strand]
            st = part.chrstart.values.astype(int)[1:]
            en = part.chrend.values.astype(int)[:-1]
            diff = st-en
            iscluster = np.array(diff >= smoothing_bases)
            indices = np.arange(len(iscluster)) #[]
            indices = indices[iscluster]

            parts = []
            old_index = 0
            for index in indices:
                parts += [part[old_index:index+1], spacer]
                old_index = index+1
            parts.append(part[old_index:])
            res += parts
            if res[-1].tail(1).size == 0:       # pragma: no cover
                pass
            elif not np.all(res[-1].tail(1).values[0]=="nan"):      # pragma: no cover
                res.append(spacer)

    res.pop()
    if return_list:
        return res
    res = pd.concat(res).reset_index(drop=True)
    return res


def peak_call(chrom, x, y, idx, strand, bedgraph, smoothing="weak"):
    res = []
    x2 = np.arange(x[0], x[-1], 1)
    m = len(x2)
    if smoothing is None:       # pragma: no cover
        smoothing = 0
    elif smoothing == "weak":     # pragma: no cover
        smoothing = m - np.sqrt(2*m)
    elif smoothing == "moderate":     # pragma: no cover
        smoothing = m
    elif smoothing == "strong":     # pragma: no cover
        smoothing = m + np.sqrt(2*m)

    try:        # pragma: no cover
        tck = splrep(x, y, s=smoothing)
        yyy = splev(x, tck, der=0)
        yder = splev(x, tck, der=1)
        ynew = splev(x2, tck, der=0)

        apex = []
        for z in range(len(yder)-1):
            a = yder[z]
            b = yder[z+1]
            if a > 0 and b < 0:
                apex.append([x[z+1], yyy[z]])

        for i in apex:
            f_x = i[0]
            f_y = i[1]
            if f_y > 0:
                out = [chrom, str(f_x), str(f_x), 'Peak_'+str(y[x.index(f_x)-1]), str(f_y), str(f_x), strand]
                out = "\t".join(out)
                res.append(out)
    except:     # pragma: no cover
        max_y = max(y)
        if max_y > 0:
            max_idx = np.argwhere(y == np.amax(y)).flatten()
            xx = np.array(x)[max_idx]

            f_x = (xx[0] + xx[-1]) // 2
            out = [chrom, str(f_x), str(f_x+1), 'Peak_'+str(max_y), str(max_y), str(f_x), strand]
            out = "\t".join(out)
            res.append(out)

    return res


def inner_join(bed_df, genome_assembly, exon_only=True):
    # ["chrom", "chrstart", "chrend", "peak", "peak_height", "peak_pos", "strand"]
    if exon_only:
        con = db.connect(get_exome_data())
    else:
        con = db.connect(get_genome_data())

    if not isinstance(genome_assembly, str):        # pragma: no cover
        genome_assembly.to_sql("custom_genome", con, index=False, if_exists="replace")
        genome_assembly = custom_genome
    bed_df.to_sql("bed_df", con, index=False, if_exists="replace")
    joined = pd.read_sql("""SELECT * FROM bed_df AS b
                         INNER JOIN {} AS g ON b.chrom = g.chrom
                         AND b.chrstart >= g.chrstart AND b.chrend <= g.chrend
                         AND b.strand = g.strand""".format(genome_assembly), con)
    res = joined.ix[:, :len(bed_df.columns)]
    res["RefSeq"] = joined.name.str.findall("[A-Z][A-Z]_[0-9]+").str.get(0)

    con.execute("DROP TABLE bed_df")
    if genome_assembly == "custom_genome":
        con.execute("DROP TABLE custom_genome")
    con.commit(); con.close(); del con

    res = pd.concat([res["RefSeq"], res["peak"], res["peak_height"], res["chrom"],
                     res["peak_pos"], res["strand"]], axis=1)
    return res
