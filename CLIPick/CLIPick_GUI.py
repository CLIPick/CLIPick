#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
#import CLIPick
from CLIPick.datasets import *
from CLIPick.preprocessing import *
from CLIPick.peak_calling import *
from CLIPick.random_ip import *
from CLIPick.report import *
from CLIPick.graphics import *
from CLIPick.__init__ import *
import datetime as dt
import matplotlib.pyplot as plt
import os, subprocess, sys

ts = str(dt.datetime.now()).split()[0]

class Ui_MainWindow(QtWidgets.QDialog):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(360, 635)
        MainWindow.setMinimumSize(QtCore.QSize(360, 635))
        MainWindow.setMaximumSize(QtCore.QSize(360, 635))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.logo_pickcheck = QtWidgets.QPushButton(self.centralwidget)
        self.logo_pickcheck.setEnabled(False)
        self.logo_pickcheck.setGeometry(QtCore.QRect(187, 27, 31, 31))
        self.logo_pickcheck.setStatusTip("")
        self.logo_pickcheck.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"font: 18pt \"Nanum Pen Script\";\n"
"color: rgb(255, 255, 255);")
        self.logo_pickcheck.setObjectName("logo_pickcheck")
        self.startbutton = QtWidgets.QPushButton(self.centralwidget)
        self.startbutton.setEnabled(True)
        self.startbutton.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.startbutton.setStatusTip("")
        self.startbutton.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.startbutton.setDefault(False)
        self.startbutton.setObjectName("startbutton")
        self.title = QtWidgets.QPushButton(self.centralwidget)
        self.title.setEnabled(False)
        self.title.setGeometry(QtCore.QRect(-70, 20, 501, 81))
        self.title.setStatusTip("")
        self.title.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-color: rgba(255, 255, 255, 0);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.title.setObjectName("title")
        self.subtitle = QtWidgets.QPushButton(self.centralwidget)
        self.subtitle.setEnabled(False)
        self.subtitle.setGeometry(QtCore.QRect(-70, 80, 501, 31))
        self.subtitle.setStatusTip("")
        self.subtitle.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"font: 20 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.subtitle.setObjectName("subtitle")
        self.background = QtWidgets.QPushButton(self.centralwidget)
        self.background.setEnabled(False)
        self.background.setGeometry(QtCore.QRect(-70, -10, 501, 711))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Shadow, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Shadow, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        gradient = QtGui.QLinearGradient(0.233776, 0.0, 0.771144, 1.0)
        gradient.setSpread(QtGui.QGradient.PadSpread)
        gradient.setCoordinateMode(QtGui.QGradient.ObjectBoundingMode)
        gradient.setColorAt(0.0, QtGui.QColor(255, 70, 76))
        gradient.setColorAt(1.0, QtGui.QColor(253, 247, 182))
        brush = QtGui.QBrush(gradient)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Shadow, brush)
        self.background.setPalette(palette)
        self.background.setStatusTip("")
        self.background.setWhatsThis("")
        self.background.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.233776, y1:0, x2:0.771144, y2:1, stop:0 rgba(255, 70, 76, 255), stop:1 rgba(253, 247, 182, 255));\n"
"border-color: rgb(255, 255, 255, 0);\n"
"font: 20 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.background.setText("")
        self.background.setObjectName("background")
        self.guide1 = QtWidgets.QPushButton(self.centralwidget)
        self.guide1.setEnabled(False)
        self.guide1.setGeometry(QtCore.QRect(-80, 260, 500, 40))
        self.guide1.setStatusTip("")
        self.guide1.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 22pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide1.setObjectName("guide1")
        self.guide2 = QtWidgets.QPushButton(self.centralwidget)
        self.guide2.setEnabled(False)
        self.guide2.setGeometry(QtCore.QRect(-80, 310, 500, 60))
        self.guide2.setStatusTip("")
        self.guide2.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 21pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide2.setObjectName("guide2")
        self.guide3 = QtWidgets.QPushButton(self.centralwidget)
        self.guide3.setEnabled(False)
        self.guide3.setGeometry(QtCore.QRect(-142, 370, 550, 60))
        self.guide3.setStatusTip("")
        self.guide3.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 22pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide3.setObjectName("guide3")
        self.horizontal_bar_start = QtWidgets.QPushButton(self.centralwidget)
        self.horizontal_bar_start.setEnabled(False)
        self.horizontal_bar_start.setGeometry(QtCore.QRect(-70, 120, 500, 31))
        self.horizontal_bar_start.setStatusTip("")
        self.horizontal_bar_start.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 22pt \"Apple SD Gothic Neo\";\n"
"color: rgb(254, 233, 242);")
        self.horizontal_bar_start.setObjectName("horizontal_bar_start")
        self.guide_steps = QtWidgets.QPushButton(self.centralwidget)
        self.guide_steps.setEnabled(False)
        self.guide_steps.setGeometry(QtCore.QRect(-100, 190, 470, 40))
        self.guide_steps.setStatusTip("")
        self.guide_steps.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 200 24pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide_steps.setObjectName("guide_steps")
        self.guide4 = QtWidgets.QPushButton(self.centralwidget)
        self.guide4.setEnabled(False)
        self.guide4.setGeometry(QtCore.QRect(-70, 430, 500, 60))
        self.guide4.setStatusTip("")
        self.guide4.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 21pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide4.setObjectName("guide4")
        self.background.raise_()
        self.title.raise_()
        self.startbutton.raise_()
        self.logo_pickcheck.raise_()
        self.subtitle.raise_()
        self.guide1.raise_()
        self.guide2.raise_()
        self.guide3.raise_()
        self.horizontal_bar_start.raise_()
        self.guide_steps.raise_()
        self.guide4.raise_()

        self.startbutton.released.connect(self.guide1.hide)
        self.startbutton.released.connect(self.guide2.hide)
        self.startbutton.released.connect(self.guide3.hide)
        self.startbutton.released.connect(self.guide_steps.hide)
        self.startbutton.released.connect(self.startbutton.hide)
        self.startbutton.released.connect(self.guide4.hide)


        # --- 1st step --- #
        self.continue_button_1st = QtWidgets.QPushButton(self.centralwidget)
        self.continue_button_1st.setEnabled(True)
        self.continue_button_1st.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.continue_button_1st.setStatusTip("")
        self.continue_button_1st.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.continue_button_1st.setDefault(False)
        self.continue_button_1st.setObjectName("continue_button_1st")
        self.guide_1st_top = QtWidgets.QPushButton(self.centralwidget)
        self.guide_1st_top.setEnabled(False)
        self.guide_1st_top.setGeometry(QtCore.QRect(-50, 140, 460, 40))
        self.guide_1st_top.setStatusTip("")
        self.guide_1st_top.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide_1st_top.setObjectName("guide_1st_top")
        self.bed_label = QtWidgets.QPushButton(self.centralwidget)
        self.bed_label.setEnabled(False)
        self.bed_label.setGeometry(QtCore.QRect(40, 221, 181, 30))
        self.bed_label.setStatusTip("")
        self.bed_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.bed_label.setObjectName("bed_label")
        self.refgene_label = QtWidgets.QPushButton(self.centralwidget)
        self.refgene_label.setEnabled(False)
        self.refgene_label.setGeometry(QtCore.QRect(40, 330, 191, 50))
        self.refgene_label.setStatusTip("")
        self.refgene_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.refgene_label.setObjectName("refgene_label")
        self.open_bed_button = QtWidgets.QPushButton(self.centralwidget)
        self.open_bed_button.setGeometry(QtCore.QRect(220, 220, 91, 32))
        self.open_bed_button.setStyleSheet("")
        self.open_bed_button.setObjectName("open_bed_button")
        self.path_shower = QtWidgets.QTextEdit(self.centralwidget)
        self.path_shower.setEnabled(False)
        self.path_shower.setGeometry(QtCore.QRect(60, 251, 241, 71))
        #self.path_shower.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.path_shower.setFocusPolicy(QtCore.Qt.NoFocus)
        self.path_shower.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.path_shower.setAcceptDrops(True)
        self.path_shower.setStyleSheet("background-color: rgba(255, 255, 255, 70);\n"
"font: 13pt \"Apple SD Gothic Neo\";\n"
"color: rgba(255, 255, 255, 217);")
        self.path_shower.setText(get_data("Tags_OL_7G1-1_2A8_RefSeq.bed"))
        #self.path_shower.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.path_shower.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.path_shower.setReadOnly(True)
        #self.path_shower.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        #self.path_shower.setClearButtonEnabled(False)
        self.path_shower.setObjectName("path_shower")

        self.exp_path_shower = QtWidgets.QTextEdit(self.centralwidget)
        self.exp_path_shower.setEnabled(False)
        self.exp_path_shower.setGeometry(QtCore.QRect(60, 248, 241, 91))
        #self.exp_path_shower.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.exp_path_shower.setFocusPolicy(QtCore.Qt.NoFocus)
        self.exp_path_shower.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.exp_path_shower.setAcceptDrops(True)
        self.exp_path_shower.setStyleSheet("background-color: rgba(255, 255, 255, 70);\n"
"font: 13pt \"Apple SD Gothic Neo\";\n"
"color: rgba(255, 255, 255, 217);")
        self.exp_path_shower.setText(get_data("p13_mouse_affy_averaged.txt"))
        #self.exp_path_shower.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.exp_path_shower.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.exp_path_shower.setReadOnly(True)
        #self.exp_path_shower.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        #self.exp_path_shower.setClearButtonEnabled(False)
        self.exp_path_shower.setObjectName("exp_path_shower")

        self.output_path_shower = QtWidgets.QTextEdit(self.centralwidget)
        self.output_path_shower.setEnabled(False)
        self.output_path_shower.setGeometry(QtCore.QRect(60, 248, 241, 51))
        #self.output_path_shower.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.output_path_shower.setFocusPolicy(QtCore.Qt.NoFocus)
        self.output_path_shower.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.output_path_shower.setAcceptDrops(True)
        self.output_path_shower.setStyleSheet("background-color: rgba(255, 255, 255, 70);\n"
"font: 13pt \"Apple SD Gothic Neo\";\n"
"color: rgba(255, 255, 255, 217);")
        self.output_path_shower.setText("/path/to/your/expression/profile")
        #self.output_path_shower.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.output_path_shower.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.output_path_shower.setReadOnly(True)
        #self.output_path_shower.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        #self.output_path_shower.setClearButtonEnabled(False)
        self.output_path_shower.setObjectName("output_path_shower")

        self.complete_button = QtWidgets.QPushButton(self.centralwidget)
        self.complete_button.setEnabled(True)
        self.complete_button.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.complete_button.setStatusTip("")
        self.complete_button.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(55, 170, 243);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.complete_button.setDefault(False)
        self.complete_button.setObjectName("complete_button")
        self.complete_button.raise_()
        self.complete_button.setVisible(False)

        self.refgene_box = QtWidgets.QComboBox(self.centralwidget)
        self.refgene_box.setGeometry(QtCore.QRect(223, 341, 100, 28))
        self.refgene_box.setEditable(False)
        self.refgene_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.refgene_box.setFrame(True)
        self.refgene_box.setObjectName("refgene_box")
        self.refgene_box.addItem("")
        self.refgene_box.addItem("")
        self.refgene_box.addItem("")
        self.refgene_box.addItem("")
        self.refgene_box.addItem("")
        self.refgene_box.addItem("")

        self.dropdup_label = QtWidgets.QPushButton(self.centralwidget)
        self.dropdup_label.setEnabled(False)
        self.dropdup_label.setGeometry(QtCore.QRect(41, 380, 191, 50))
        self.dropdup_label.setStatusTip("")
        self.dropdup_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.dropdup_label.setObjectName("dropdup_label")

        self.dropdup_box = QtWidgets.QComboBox(self.centralwidget)
        self.dropdup_box.setGeometry(QtCore.QRect(228, 391, 95, 28))
        self.dropdup_box.setEditable(False)
        self.dropdup_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.dropdup_box.setFrame(True)
        self.dropdup_box.setObjectName("dropdup_box")
        self.dropdup_box.addItem("")
        self.dropdup_box.addItem("")

        self.smoothing_label = QtWidgets.QPushButton(self.centralwidget)
        self.smoothing_label.setEnabled(False)
        self.smoothing_label.setGeometry(QtCore.QRect(50, 410, 191, 50))
        self.smoothing_label.setStatusTip("")
        self.smoothing_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.smoothing_label.setObjectName("smoothing_label")

        self.smoothing_box = QtWidgets.QComboBox(self.centralwidget)
        self.smoothing_box.setGeometry(QtCore.QRect(243, 421, 80, 28))
        self.smoothing_box.setEditable(False)
        self.smoothing_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.smoothing_box.setFrame(True)
        self.smoothing_box.setObjectName("smoothing_box")
        self.smoothing_box.addItem("")
        self.smoothing_box.addItem("")
        self.smoothing_box.addItem("")
        self.smoothing_box.addItem("")

        self.insert_label = QtWidgets.QPushButton(self.centralwidget)
        self.insert_label.setEnabled(False)
        self.insert_label.setGeometry(QtCore.QRect(50, 440, 182, 50))
        self.insert_label.setStatusTip("")
        self.insert_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.insert_label.setObjectName("insert_label")

        self.insert_box = QtWidgets.QComboBox(self.centralwidget)
        self.insert_box.setGeometry(QtCore.QRect(243, 451, 80, 28))
        self.insert_box.setEditable(True)
        self.insert_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.insert_box.setFrame(True)
        self.insert_box.setObjectName("insert_box")
        self.insert_box.addItem("")
        self.insert_box.addItem("")
        self.insert_box.addItem("")

        #self.exononly_box = QtWidgets.QComboBox(self.centralwidget)
        #self.exononly_box.setGeometry(QtCore.QRect(223, 405, 100, 28))
        #self.exononly_box.setEditable(False)
        #self.exononly_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        #self.exononly_box.setFrame(True)
        #self.exononly_box.setObjectName("exononly_box")
        #self.exononly_box.addItem("")
        #self.exononly_box.addItem("")

        self.continue_button_1st.released.connect(self.guide_1st_top.hide)
        self.continue_button_1st.released.connect(self.bed_label.hide)
        self.continue_button_1st.released.connect(self.open_bed_button.hide)
        self.continue_button_1st.released.connect(self.path_shower.hide)
        self.continue_button_1st.released.connect(self.exp_path_shower.show)
        self.continue_button_1st.released.connect(self.refgene_label.hide)
        self.continue_button_1st.released.connect(self.dropdup_label.hide)
        self.continue_button_1st.released.connect(self.smoothing_label.hide)
        self.continue_button_1st.released.connect(self.insert_label.hide)
        self.continue_button_1st.released.connect(self.continue_button_1st.hide)
        self.continue_button_1st.released.connect(self.refgene_box.hide)
        self.continue_button_1st.released.connect(self.dropdup_box.hide)
        self.continue_button_1st.released.connect(self.smoothing_box.hide)
        self.continue_button_1st.released.connect(self.insert_box.hide)
        #self.continue_button_1st.released.connect(self.exononly_box.hide)

        self.guide_1st_top.setVisible(False)
        self.bed_label.setVisible(False)
        self.open_bed_button.setVisible(False)
        self.path_shower.setVisible(False)
        self.exp_path_shower.setVisible(False)
        self.output_path_shower.setVisible(False)
        self.refgene_label.setVisible(False)
        self.dropdup_label.setVisible(False)
        self.smoothing_label.setVisible(False)
        self.insert_label.setVisible(False)
        self.refgene_box.setVisible(False)
        self.dropdup_box.setVisible(False)
        self.smoothing_box.setVisible(False)
        self.insert_box.setVisible(False)
        #self.exononly_box.setVisible(False)
        self.continue_button_1st.setVisible(False)
        self.startbutton.released.connect(self.guide_1st_top.show)
        self.startbutton.released.connect(self.bed_label.show)
        self.startbutton.released.connect(self.open_bed_button.show)
        self.startbutton.released.connect(self.path_shower.show)
        self.startbutton.released.connect(self.refgene_label.show)
        self.startbutton.released.connect(self.dropdup_label.show)
        self.startbutton.released.connect(self.smoothing_label.show)
        self.startbutton.released.connect(self.insert_label.show)
        self.startbutton.released.connect(self.refgene_box.show)
        self.startbutton.released.connect(self.dropdup_box.show)
        self.startbutton.released.connect(self.smoothing_box.show)
        self.startbutton.released.connect(self.insert_box.show)
        #self.startbutton.released.connect(self.exononly_box.show)
        self.startbutton.released.connect(self.continue_button_1st.show)

        self.open_bed_button.released.connect(self.show_dialog_clip_bed)


        # --- 2nd step --- #
        self.continue_button_2nd = QtWidgets.QPushButton(self.centralwidget)
        self.continue_button_2nd.setEnabled(True)
        self.continue_button_2nd.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.continue_button_2nd.setStatusTip("")
        self.continue_button_2nd.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.continue_button_2nd.setDefault(False)
        self.continue_button_2nd.setObjectName("continue_button_2nd")
        self.guide_2nd_top = QtWidgets.QPushButton(self.centralwidget)
        self.guide_2nd_top.setEnabled(False)
        self.guide_2nd_top.setGeometry(QtCore.QRect(-50, 140, 460, 40))
        self.guide_2nd_top.setStatusTip("")
        self.guide_2nd_top.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide_2nd_top.setObjectName("guide_2nd_top")
        self.expression_label = QtWidgets.QPushButton(self.centralwidget)
        self.expression_label.setEnabled(False)
        self.expression_label.setGeometry(QtCore.QRect(40, 210, 200, 30))
        self.expression_label.setStatusTip("")
        self.expression_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.expression_label.setObjectName("expression_label")
        self.open_expression_button = QtWidgets.QPushButton(self.centralwidget)
        self.open_expression_button.setGeometry(QtCore.QRect(240, 210, 80, 32))
        self.open_expression_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.open_expression_button.setStyleSheet("")
        self.open_expression_button.setObjectName("open_expression_button")
        #self.index_label = QtWidgets.QPushButton(self.centralwidget)
        #self.index_label.setEnabled(False)
        #self.index_label.setGeometry(QtCore.QRect(39, 320, 130, 30))
        #self.index_label.setStatusTip("")
        #self.index_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
#"border-bottom-color: rgba(255, 255, 255, 0);\n"
#"border-color: rgb(255, 255, 255, 0);\n"
#"\n"
#"font: 10 20pt \"Apple SD Gothic Neo\";\n"
#"color: rgb(255, 255, 255);")
        #self.index_label.setObjectName("index_label")
        #self.index_refseq_box = QtWidgets.QSpinBox(self.centralwidget)
        #self.index_refseq_box.setGeometry(QtCore.QRect(174, 322, 40, 24))
        #self.index_refseq_box.setFocusPolicy(QtCore.Qt.NoFocus)
        #self.index_refseq_box.setMaximum(20)
        #self.index_refseq_box.setObjectName("index_refseq_box")
        #self.index_length_box = QtWidgets.QSpinBox(self.centralwidget)
        #self.index_length_box.setGeometry(QtCore.QRect(225, 322, 40, 24))
        #self.index_length_box.setFocusPolicy(QtCore.Qt.NoFocus)
        #self.index_length_box.setMaximum(20)
        #self.index_length_box.setProperty("value", 1)
        #self.index_length_box.setObjectName("index_length_box")
        #self.index_expression_box = QtWidgets.QSpinBox(self.centralwidget)
        #self.index_expression_box.setGeometry(QtCore.QRect(277, 322, 40, 24))
        #self.index_expression_box.setFocusPolicy(QtCore.Qt.NoFocus)
        #self.index_expression_box.setMaximum(20)
        #self.index_expression_box.setProperty("value", 2)
        #self.index_expression_box.setObjectName("index_expression_box")
        self.dtype_label = QtWidgets.QPushButton(self.centralwidget)
        self.dtype_label.setEnabled(False)
        self.dtype_label.setGeometry(QtCore.QRect(40, 355, 120, 30))
        self.dtype_label.setStatusTip("")
        self.dtype_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.dtype_label.setObjectName("dtype_label")
        self.dtype_box = QtWidgets.QComboBox(self.centralwidget)
        self.dtype_box.setGeometry(QtCore.QRect(170, 355, 111, 28))
        self.dtype_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.dtype_box.setEditable(False)
        self.dtype_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.dtype_box.setFrame(True)
        self.dtype_box.setObjectName("dtype_box")
        self.dtype_box.addItem("")
        self.dtype_box.addItem("")
        self.num_reads_label = QtWidgets.QPushButton(self.centralwidget)
        self.num_reads_label.setEnabled(False)
        self.num_reads_label.setGeometry(QtCore.QRect(-50, 420, 460, 30))
        self.num_reads_label.setStatusTip("")
        self.num_reads_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 19pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.num_reads_label.setObjectName("num_reads_label")
        self.num_reads_box = QtWidgets.QComboBox(self.centralwidget)
        self.num_reads_box.setGeometry(QtCore.QRect(170, 450, 158, 28))
        self.num_reads_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.num_reads_box.setEditable(False)
        self.num_reads_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.num_reads_box.setFrame(True)
        self.num_reads_box.setObjectName("num_reads_box")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")
        self.num_reads_box.addItem("")

        self.continue_button_2nd.released.connect(self.guide_2nd_top.hide)
        self.continue_button_2nd.released.connect(self.num_reads_label.hide)
        self.continue_button_2nd.released.connect(self.dtype_box.hide)
        self.continue_button_2nd.released.connect(self.dtype_label.hide)
        #self.continue_button_2nd.released.connect(self.index_refseq_box.hide)
        #self.continue_button_2nd.released.connect(self.index_length_box.hide)
        #self.continue_button_2nd.released.connect(self.index_expression_box.hide)
        #self.continue_button_2nd.released.connect(self.index_label.hide)
        self.continue_button_2nd.released.connect(self.open_expression_button.hide)
        self.continue_button_2nd.released.connect(self.expression_label.hide)
        self.continue_button_2nd.released.connect(self.continue_button_2nd.hide)
        self.continue_button_2nd.released.connect(self.num_reads_box.hide)
        self.continue_button_2nd.released.connect(self.exp_path_shower.hide)
        self.refgene_box.activated['QString'].connect(self.show_dialog_refgene)
        self.dropdup_box.activated['QString'].connect(self.show_dialog_refgene)
        self.smoothing_box.activated['QString'].connect(self.show_dialog_refgene)
        self.insert_box.activated['QString'].connect(self.show_dialog_refgene)

        self.guide_2nd_top.setVisible(False)
        self.expression_label.setVisible(False)
        self.open_expression_button.setVisible(False)
        #self.index_label.setVisible(False)
        #self.index_refseq_box.setVisible(False)
        #self.index_length_box.setVisible(False)
        #self.index_expression_box.setVisible(False)
        self.dtype_label.setVisible(False)
        self.dtype_box.setVisible(False)
        self.num_reads_label.setVisible(False)
        self.num_reads_box.setVisible(False)
        self.continue_button_2nd.setVisible(False)
        self.continue_button_1st.released.connect(self.guide_2nd_top.show)
        self.continue_button_1st.released.connect(self.expression_label.show)
        self.continue_button_1st.released.connect(self.open_expression_button.show)
        #self.continue_button_1st.released.connect(self.index_label.show)
        #self.continue_button_1st.released.connect(self.index_refseq_box.show)
        #self.continue_button_1st.released.connect(self.index_length_box.show)
        #self.continue_button_1st.released.connect(self.index_expression_box.show)
        self.continue_button_1st.released.connect(self.dtype_label.show)
        self.continue_button_1st.released.connect(self.dtype_box.show)
        self.continue_button_1st.released.connect(self.num_reads_label.show)
        self.continue_button_1st.released.connect(self.num_reads_box.show)
        self.continue_button_1st.released.connect(self.continue_button_2nd.show)

        self.open_expression_button.released.connect(self.show_dialog_exp_interval)


        # --- 3rd step --- #
        self.continue_button_3rd = QtWidgets.QPushButton(self.centralwidget)
        self.continue_button_3rd.setEnabled(True)
        self.continue_button_3rd.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.continue_button_3rd.setStatusTip("")
        self.continue_button_3rd.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.continue_button_3rd.setDefault(False)
        self.continue_button_3rd.setObjectName("continue_button_3rd")
        self.guide_3rd_top = QtWidgets.QPushButton(self.centralwidget)
        self.guide_3rd_top.setEnabled(False)
        self.guide_3rd_top.setGeometry(QtCore.QRect(-50, 140, 460, 40))
        self.guide_3rd_top.setStatusTip("")
        self.guide_3rd_top.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide_3rd_top.setObjectName("guide_3rd_top")
        self.fragsize_label = QtWidgets.QPushButton(self.centralwidget)
        self.fragsize_label.setEnabled(False)
        self.fragsize_label.setGeometry(QtCore.QRect(43, 180, 300, 30))
        self.fragsize_label.setStatusTip("")
        self.fragsize_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 50 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.fragsize_label.setObjectName("fragsize_label")
        self.puri_label = QtWidgets.QPushButton(self.centralwidget)
        self.puri_label.setEnabled(False)
        self.puri_label.setGeometry(QtCore.QRect(37, 205, 200, 30))
        self.puri_label.setStatusTip("")
        self.puri_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 50 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.puri_label.setObjectName("puri_label")
        self.sequencing_label = QtWidgets.QPushButton(self.centralwidget)
        self.sequencing_label.setEnabled(False)
        self.sequencing_label.setGeometry(QtCore.QRect(45, 305, 200, 30))
        self.sequencing_label.setStatusTip("")
        self.sequencing_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.sequencing_label.setObjectName("sequencing_label")
        self.isrip_label = QtWidgets.QPushButton(self.centralwidget)
        self.isrip_label.setEnabled(False)
        self.isrip_label.setGeometry(QtCore.QRect(40, 410, 200, 30))
        self.isrip_label.setStatusTip("")
        self.isrip_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.isrip_label.setObjectName("isrip_label")
        self.num_iter_label = QtWidgets.QPushButton(self.centralwidget)
        self.num_iter_label.setEnabled(False)
        self.num_iter_label.setGeometry(QtCore.QRect(58, 440, 180, 30))
        self.num_iter_label.setStatusTip("")
        self.num_iter_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.num_iter_label.setObjectName("num_iter_label")
        self.lower_label = QtWidgets.QPushButton(self.centralwidget)
        self.lower_label.setEnabled(False)
        self.lower_label.setGeometry(QtCore.QRect(57, 235, 80, 30))
        self.lower_label.setStatusTip("")
        self.lower_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.lower_label.setObjectName("lower_label")
        self.upper_label = QtWidgets.QPushButton(self.centralwidget)
        self.upper_label.setEnabled(False)
        self.upper_label.setGeometry(QtCore.QRect(58, 263, 80, 30))
        self.upper_label.setStatusTip("")
        self.upper_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.upper_label.setObjectName("upper_label")
        self.readtype_label = QtWidgets.QPushButton(self.centralwidget)
        self.readtype_label.setEnabled(False)
        self.readtype_label.setGeometry(QtCore.QRect(67, 335, 90, 30))
        self.readtype_label.setStatusTip("")
        self.readtype_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.readtype_label.setObjectName("readtype_label")
        self.readlength_label = QtWidgets.QPushButton(self.centralwidget)
        self.readlength_label.setEnabled(False)
        self.readlength_label.setGeometry(QtCore.QRect(68, 365, 100, 30))
        self.readlength_label.setStatusTip("")
        self.readlength_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.readlength_label.setObjectName("readlength_label")
        self.readtype_box = QtWidgets.QComboBox(self.centralwidget)
        self.readtype_box.setGeometry(QtCore.QRect(167, 335, 102, 28))
        self.readtype_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.readtype_box.setEditable(False)
        self.readtype_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.readtype_box.setFrame(True)
        self.readtype_box.setObjectName("readtype_box")
        self.readtype_box.addItem("")
        self.readtype_box.addItem("")
        self.ntd_label = QtWidgets.QPushButton(self.centralwidget)
        self.ntd_label.setEnabled(False)
        self.ntd_label.setGeometry(QtCore.QRect(212, 365, 40, 30))
        self.ntd_label.setStatusTip("")
        self.ntd_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 17pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.ntd_label.setObjectName("ntd_label")
        self.ntd_label_2 = QtWidgets.QPushButton(self.centralwidget)
        self.ntd_label_2.setEnabled(False)
        self.ntd_label_2.setGeometry(QtCore.QRect(195, 233, 40, 30))
        self.ntd_label_2.setStatusTip("")
        self.ntd_label_2.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 17pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.ntd_label_2.setObjectName("ntd_label_2")
        self.ntd_label_3 = QtWidgets.QPushButton(self.centralwidget)
        self.ntd_label_3.setEnabled(False)
        self.ntd_label_3.setGeometry(QtCore.QRect(195, 263, 40, 30))
        self.ntd_label_3.setStatusTip("")
        self.ntd_label_3.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 17pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.ntd_label_3.setObjectName("ntd_label_3")
        self.num_iter_box = QtWidgets.QComboBox(self.centralwidget)
        self.num_iter_box.setGeometry(QtCore.QRect(228, 440, 70, 28))
        self.num_iter_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.num_iter_box.setEditable(True)
        self.num_iter_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.num_iter_box.setFrame(True)
        self.num_iter_box.setObjectName("num_iter_box")
        self.num_iter_box.addItem("")
        self.readlength_box = QtWidgets.QComboBox(self.centralwidget)
        self.readlength_box.setGeometry(QtCore.QRect(167, 365, 50, 28))
        self.readlength_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.readlength_box.setEditable(True)
        self.readlength_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.readlength_box.setFrame(True)
        self.readlength_box.setObjectName("readlength_box")
        self.readlength_box.addItem("")
        self.fragsize_box = QtWidgets.QComboBox(self.centralwidget)
        self.fragsize_box.setGeometry(QtCore.QRect(243, 180, 60, 28))
        self.fragsize_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.fragsize_box.setEditable(True)
        self.fragsize_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.fragsize_box.setFrame(True)
        self.fragsize_box.setObjectName("fragsize_box")
        self.fragsize_box.addItem("")
        self.lower_box = QtWidgets.QComboBox(self.centralwidget)
        self.lower_box.setGeometry(QtCore.QRect(140, 233, 60, 28))
        self.lower_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.lower_box.setEditable(True)
        self.lower_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.lower_box.setFrame(True)
        self.lower_box.setObjectName("lower_box")
        self.lower_box.addItem("")
        self.upper_box = QtWidgets.QComboBox(self.centralwidget)
        self.upper_box.setGeometry(QtCore.QRect(140, 263, 60, 28))
        self.upper_box.setFocusPolicy(QtCore.Qt.NoFocus)
        self.upper_box.setEditable(True)
        self.upper_box.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.upper_box.setFrame(True)
        self.upper_box.setObjectName("upper_box")
        self.upper_box.addItem("")
        self.window_label = QtWidgets.QPushButton(self.centralwidget)
        self.window_label.setEnabled(False)
        self.window_label.setGeometry(QtCore.QRect(65, 495, 230, 30))
        self.window_label.setStatusTip("")
        self.window_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.window_label.setObjectName("window_label")
        self.window_box = QtWidgets.QComboBox(self.centralwidget)
        self.window_box.setGeometry(QtCore.QRect(158, 497, 51, 26))
        self.window_box.setObjectName("window_box")
        self.window_box.addItem("")
        self.window_box.addItem("")
        self.window_box.addItem("")

        self.alpha_label = QtWidgets.QPushButton(self.centralwidget)
        self.alpha_label.setEnabled(False)
        self.alpha_label.setGeometry(QtCore.QRect(5, 468, 230, 30))
        self.alpha_label.setStatusTip("")
        self.alpha_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 18pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.alpha_label.setObjectName("alpha_label")
        self.alpha_box = QtWidgets.QComboBox(self.centralwidget)
        self.alpha_box.setGeometry(QtCore.QRect(165, 468, 65, 26))
        self.alpha_box.setObjectName("alpha_box")
        self.alpha_box.addItem("")
        self.alpha_box.addItem("")
        self.alpha_box.addItem("")

        self.continue_button_3rd.released.connect(self.guide_3rd_top.hide)
        self.continue_button_3rd.released.connect(self.continue_button_3rd.hide)
        self.continue_button_3rd.released.connect(self.num_iter_box.hide)
        self.continue_button_3rd.released.connect(self.num_iter_label.hide)
        self.continue_button_3rd.released.connect(self.readlength_label.hide)
        self.continue_button_3rd.released.connect(self.readtype_label.hide)
        self.continue_button_3rd.released.connect(self.ntd_label_3.hide)
        self.continue_button_3rd.released.connect(self.sequencing_label.hide)
        self.continue_button_3rd.released.connect(self.isrip_label.hide)
        self.continue_button_3rd.released.connect(self.ntd_label_2.hide)
        self.continue_button_3rd.released.connect(self.ntd_label.hide)
        self.continue_button_3rd.released.connect(self.upper_box.hide)
        self.continue_button_3rd.released.connect(self.upper_label.hide)
        self.continue_button_3rd.released.connect(self.lower_box.hide)
        self.continue_button_3rd.released.connect(self.fragsize_box.hide)
        self.continue_button_3rd.released.connect(self.lower_label.hide)
        self.continue_button_3rd.released.connect(self.puri_label.hide)
        self.continue_button_3rd.released.connect(self.fragsize_label.hide)
        self.continue_button_3rd.released.connect(self.readtype_box.hide)
        self.continue_button_3rd.released.connect(self.readlength_box.hide)

        self.guide_3rd_top.setVisible(False)
        self.puri_label.setVisible(False)
        self.fragsize_label.setVisible(False)
        self.lower_label.setVisible(False)
        self.lower_box.setVisible(False)
        self.fragsize_box.setVisible(False)
        self.upper_label.setVisible(False)
        self.upper_box.setVisible(False)
        self.ntd_label.setVisible(False)
        self.ntd_label_2.setVisible(False)
        self.ntd_label_3.setVisible(False)
        self.sequencing_label.setVisible(False)
        self.isrip_label.setVisible(False)
        self.readtype_label.setVisible(False)
        self.readtype_box.setVisible(False)
        self.readlength_label.setVisible(False)
        self.readlength_box.setVisible(False)
        self.num_iter_label.setVisible(False)
        self.num_iter_box.setVisible(False)
        self.continue_button_3rd.setVisible(False)
        self.window_label.setVisible(False)
        self.window_box.setVisible(False)
        self.alpha_label.setVisible(False)
        self.alpha_box.setVisible(False)
        self.continue_button_2nd.released.connect(self.window_label.show)
        self.continue_button_3rd.released.connect(self.window_label.hide)
        self.continue_button_2nd.released.connect(self.alpha_label.show)
        self.continue_button_3rd.released.connect(self.alpha_label.hide)
        self.continue_button_2nd.released.connect(self.window_box.show)
        self.continue_button_3rd.released.connect(self.window_box.hide)
        self.continue_button_2nd.released.connect(self.alpha_box.show)
        self.continue_button_3rd.released.connect(self.alpha_box.hide)
        self.continue_button_2nd.released.connect(self.guide_3rd_top.show)
        self.continue_button_2nd.released.connect(self.puri_label.show)
        self.continue_button_2nd.released.connect(self.fragsize_label.show)
        self.continue_button_2nd.released.connect(self.lower_label.show)
        self.continue_button_2nd.released.connect(self.lower_box.show)
        self.continue_button_2nd.released.connect(self.fragsize_box.show)
        self.continue_button_2nd.released.connect(self.upper_label.show)
        self.continue_button_2nd.released.connect(self.upper_box.show)
        self.continue_button_2nd.released.connect(self.ntd_label.show)
        self.continue_button_2nd.released.connect(self.ntd_label_2.show)
        self.continue_button_2nd.released.connect(self.ntd_label_3.show)
        self.continue_button_2nd.released.connect(self.sequencing_label.show)
        self.continue_button_2nd.released.connect(self.isrip_label.show)
        self.continue_button_2nd.released.connect(self.readtype_label.show)
        self.continue_button_2nd.released.connect(self.readtype_box.show)
        self.continue_button_2nd.released.connect(self.readlength_label.show)
        self.continue_button_2nd.released.connect(self.readlength_box.show)
        self.continue_button_2nd.released.connect(self.num_iter_label.show)
        self.continue_button_2nd.released.connect(self.num_iter_box.show)
        self.continue_button_2nd.released.connect(self.continue_button_3rd.show)


        # --- 4th step --- #
        self.run_button = QtWidgets.QPushButton(self.centralwidget)
        self.run_button.setEnabled(True)
        self.run_button.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.run_button.setStatusTip("")
        self.run_button.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.run_button.setDefault(False)
        self.run_button.setObjectName("run_button")
        self.guide_4th_top = QtWidgets.QPushButton(self.centralwidget)
        self.guide_4th_top.setEnabled(False)
        self.guide_4th_top.setGeometry(QtCore.QRect(-50, 140, 460, 40))
        self.guide_4th_top.setStatusTip("")
        self.guide_4th_top.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.guide_4th_top.setObjectName("guide_4th_top")
        self.signi_peak_label = QtWidgets.QPushButton(self.centralwidget)
        self.signi_peak_label.setEnabled(False)
        self.signi_peak_label.setGeometry(QtCore.QRect(70, 230, 170, 30))
        self.signi_peak_label.setStatusTip("")
        self.signi_peak_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.signi_peak_label.setObjectName("signi_peak_label")
        self.cluster_label = QtWidgets.QPushButton(self.centralwidget)
        self.cluster_label.setEnabled(False)
        self.cluster_label.setGeometry(QtCore.QRect(79, 350, 140, 30))
        self.cluster_label.setStatusTip("")
        self.cluster_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.cluster_label.setObjectName("cluster_label")
        self.peak_overview_label = QtWidgets.QPushButton(self.centralwidget)
        self.peak_overview_label.setEnabled(False)
        self.peak_overview_label.setGeometry(QtCore.QRect(45, 380, 200, 30))
        self.peak_overview_label.setStatusTip("")
        self.peak_overview_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.peak_overview_label.setObjectName("peak_overview_label")
        self.clipick_summary_label = QtWidgets.QPushButton(self.centralwidget)
        self.clipick_summary_label.setEnabled(False)
        self.clipick_summary_label.setGeometry(QtCore.QRect(58, 260, 200, 30))
        self.clipick_summary_label.setStatusTip("")
        self.clipick_summary_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.clipick_summary_label.setObjectName("clipick_summary_label")
        self.peak_call_label = QtWidgets.QPushButton(self.centralwidget)
        self.peak_call_label.setEnabled(False)
        self.peak_call_label.setGeometry(QtCore.QRect(83, 410, 130, 30))
        self.peak_call_label.setStatusTip("")
        self.peak_call_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 20pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.peak_call_label.setObjectName("peak_call_label")
        self.window_check = QtWidgets.QCheckBox(self.centralwidget)
        self.window_check.setGeometry(QtCore.QRect(68, 235, 20, 20))
        self.window_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.window_check.setText("")
        self.window_check.setChecked(True)
        self.window_check.setEnabled(False)
        self.window_check.setObjectName("window_check")
        self.cluster_check = QtWidgets.QCheckBox(self.centralwidget)
        self.cluster_check.setGeometry(QtCore.QRect(68, 355, 20, 20))
        self.cluster_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.cluster_check.setText("")
        self.cluster_check.setObjectName("cluster_check")
        self.peak_overview_check = QtWidgets.QCheckBox(self.centralwidget)
        self.peak_overview_check.setGeometry(QtCore.QRect(68, 385, 20, 20))
        self.peak_overview_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.peak_overview_check.setText("")
        self.peak_overview_check.setObjectName("peak_overview_check")
        self.clipick_summary_check = QtWidgets.QCheckBox(self.centralwidget)
        self.clipick_summary_check.setGeometry(QtCore.QRect(68, 265, 20, 20))
        self.clipick_summary_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.clipick_summary_check.setChecked(True)
        self.clipick_summary_check.setEnabled(False)
        self.clipick_summary_check.setText("")
        self.clipick_summary_check.setObjectName("clipick_summary_check")
        self.peak_calling_check = QtWidgets.QCheckBox(self.centralwidget)
        self.peak_calling_check.setGeometry(QtCore.QRect(68, 415, 20, 20))
        self.peak_calling_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.peak_calling_check.setText("")
        self.peak_calling_check.setObjectName("peak_calling_check")
        self.signi_peak_sup_label = QtWidgets.QPushButton(self.centralwidget)
        self.signi_peak_sup_label.setEnabled(False)
        self.signi_peak_sup_label.setGeometry(QtCore.QRect(30, 200, 200, 30))
        self.signi_peak_sup_label.setStatusTip("")
        self.signi_peak_sup_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 22pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.signi_peak_sup_label.setObjectName("signi_peak_sup_label")
        self.figures_label = QtWidgets.QPushButton(self.centralwidget)
        self.figures_label.setEnabled(False)
        self.figures_label.setGeometry(QtCore.QRect(54, 320, 70, 30))
        self.figures_label.setStatusTip("")
        self.figures_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 10 22pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.figures_label.setObjectName("figures_label")
        self.progressbar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressbar.setGeometry(QtCore.QRect(30, 503, 301, 20))
        self.progressbar.setObjectName("progressbar")
        self.processing_label = QtWidgets.QPushButton(self.centralwidget)
        self.processing_label.setEnabled(False)
        self.processing_label.setGeometry(QtCore.QRect(30, 480, 300, 30))
        self.processing_label.setStatusTip("")
        self.processing_label.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 14pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.processing_label.setObjectName("processing_label")

        self.processing_label2 = QtWidgets.QPushButton(self.centralwidget)
        self.processing_label2.setEnabled(False)
        self.processing_label2.setGeometry(QtCore.QRect(110, 480, 140, 30))
        self.processing_label2.setStatusTip("")
        self.processing_label2.setStyleSheet("background-color: rgb(252, 43, 7, 0);\n"
"border-bottom-color: rgba(255, 255, 255, 0);\n"
"border-color: rgb(255, 255, 255, 0);\n"
"\n"
"font: 100 14pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);")
        self.processing_label2.setObjectName("processing_label2")

        self.running_button = QtWidgets.QPushButton(self.centralwidget)
        self.running_button.setEnabled(False)
        self.running_button.setGeometry(QtCore.QRect(-70, 530, 501, 111))
        self.running_button.setStatusTip("")
        self.running_button.setStyleSheet("border-color: rgb(255, 255, 255, 0);\n"
"background-color: rgb(250, 128, 113);\n"
"font: 10 48pt \"Apple SD Gothic Neo\";\n"
"color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(226, 101, 86);\n"
"selection-color: rgba(255, 255, 255, 0);")
        self.running_button.setDefault(False)
        self.running_button.setObjectName("running_button")

        self.running_button.setVisible(False)

        MainWindow.setCentralWidget(self.centralwidget)
        #self.menubar = QtWidgets.QMenuBar(MainWindow)
        #self.menubar.setGeometry(QtCore.QRect(0, 0, 360, 22))
        #self.menubar.setObjectName("menubar")
        #MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)

        self.guide_4th_top.setVisible(False)
        self.signi_peak_sup_label.setVisible(False)
        self.signi_peak_label.setVisible(False)
        self.window_check.setVisible(False)
        self.figures_label.setVisible(False)
        self.cluster_label.setVisible(False)
        self.cluster_check.setVisible(False)
        self.peak_overview_label.setVisible(False)
        self.clipick_summary_label.setVisible(False)
        self.peak_overview_check.setVisible(False)
        self.clipick_summary_check.setVisible(False)
        self.peak_call_label.setVisible(False)
        self.peak_calling_check.setVisible(False)
        self.progressbar.setVisible(False)
        self.processing_label.setVisible(False)
        self.processing_label2.setVisible(False)
        self.run_button.setVisible(False)
        self.continue_button_3rd.released.connect(self.guide_4th_top.show)
        self.continue_button_3rd.released.connect(self.signi_peak_sup_label.show)
        self.continue_button_3rd.released.connect(self.signi_peak_label.show)
        self.continue_button_3rd.released.connect(self.window_check.show)
        self.continue_button_3rd.released.connect(self.figures_label.show)
        self.continue_button_3rd.released.connect(self.cluster_label.show)
        self.continue_button_3rd.released.connect(self.cluster_check.show)
        self.continue_button_3rd.released.connect(self.peak_overview_label.show)
        self.continue_button_3rd.released.connect(self.clipick_summary_label.show)
        self.continue_button_3rd.released.connect(self.peak_overview_check.show)
        self.continue_button_3rd.released.connect(self.clipick_summary_check.show)
        self.continue_button_3rd.released.connect(self.peak_call_label.show)
        self.continue_button_3rd.released.connect(self.peak_calling_check.show)
        self.continue_button_3rd.released.connect(self.run_button.show)

        self.continue_button_3rd.released.connect(self.progressbar.show)
        self.continue_button_3rd.released.connect(self.processing_label.show)
        self.run_button.clicked.connect(self.run_button.hide)
        self.run_button.clicked.connect(self.processing_label.hide)
        self.run_button.clicked.connect(self.processing_label2.show)
        self.run_button.clicked.connect(self.running_button.show)

        self.run_button.clicked.connect(self.show_dialog_output)
        self.run_button.clicked.connect(self.start_CLIPick)
        self.complete_button.clicked.connect(self.open_finder_result)
        self.complete_button.clicked.connect(self.quit_app)

        #QtCore.QMetaObject.connectSlotsByName(MainWindow)
    # --- BACKEND CLIPick CODE --- #

    def test_CLIPick(self):
        self.progress = 0

        # 0. set variables
        clip_bed_name = str(self.path_shower.toPlainText())
        genome_assembly = str(self.refgene_box.currentText())
        #exon_only = str(self.exononly_box.currentText())

        exp_name = str(self.exp_path_shower.toPlainText())
        #index_refseq = int(self.index_refseq_box.value())
        #index_length = int(self.index_length_box.value())
        #index_expression = int(self.index_expression_box.value())
        dtype = str(self.dtype_box.currentText())
        prebuilt = self.num_reads_box.currentText()

        fragsize = int(self.fragsize_box.currentText())
        lower = int(self.lower_box.currentText())
        upper = int(self.upper_box.currentText())
        readtype = str(self.readtype_box.currentText())[0]
        readlength = int(self.readlength_box.currentText())
        n_iter = int(self.num_iter_box.currentText())

        whattoshow = {
            "cluster plot": self.cluster_check.isChecked(),
            "summary plot": self.peak_overview_check.isChecked(),
            "peak plot": self.peak_calling_check.isChecked(),
        }

        # 1. peak calling
        bgc = BedGraphConverter()
        bgc.fit(clip_bed_name, genome_assembly)
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()  ##################################

        bedgraph = bgc.transform()
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()  ##################################

        self.running_button.hide()
        self.complete_button.show()

    def start_CLIPick(self):
        self.progress = 0
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        # 0. set variables
        clip_bed_name = str(self.path_shower.toPlainText())
        genome_assembly = str(self.refgene_box.currentText())
        if genome_assembly == "others..":
            genome_assembly = self.user_genome

        dropdup = str(self.dropdup_box.currentText())
        if dropdup == "by start & end positions":
            drop_st_en = True
        else:
            drop_st_en = False

        smoothing = str(self.smoothing_box.currentText())
        if smoothing == "Don't":
            smoothing = None
        elif smoothing == "Weak":
            smoothing = "weak"
        elif smoothing == "Moderate":
            smoothing = "moderate"
        else:
            smoothing = "strong"

        insert = str(self.insert_box.currentText())
        if insert == "Don't":
            insert = None
        else:
            if not insert.isnumeric():
                raise ValueError("'insert' should be a positive integer. Invalid value: {}".format(insert))
            insert = int(insert)

        #exon_only = str(self.exononly_box.currentText())
        #if exon_only == "mRNA":
        #    exon_only = True
        #else: exon_only = False
        prebuilt = self.num_reads_box.currentText()

        if prebuilt == "No: Use user's data":
            prebuilt = str(self.exp_path_shower.toPlainText())
            dtype = str(self.dtype_box.currentText())

        fragsize = int(self.fragsize_box.currentText())
        lower = int(self.lower_box.currentText())
        upper = int(self.upper_box.currentText())
        readtype = str(self.readtype_box.currentText())[0]
        readlength = int(self.readlength_box.currentText())
        n_iter = int(self.num_iter_box.currentText())

        whattoshow = {
            "cluster plot": self.cluster_check.isChecked(),
            "summary plot": self.peak_overview_check.isChecked(),
            "peak plot": self.peak_calling_check.isChecked(),
        }
        self.window_level_percent = str(self.window_box.currentText())
        window_level = float(self.window_level_percent)/100.
        self.alpha = float(self.alpha_box.currentText())

        # 1. peak calling
        bgc = BedGraphConverter()
        bgc.fit(clip_bed_name, genome_assembly, insert=insert, drop_st_en=drop_st_en)
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()
        bedgraph = bgc.transform(write_file=False)
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        pc = PeakCaller()
        pc.fit(bedgraph, genome_assembly)
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()
        called_peaks = pc.call(write_file=False, smoothing=smoothing)
        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        # 2. in silico Random IP
        if not prebuilt.startswith("hsap"):
            if dtype == "RNA-Seq":
                baseline = load_custom_rnaseq(prebuilt, genome_assembly)
                cal = ExpTagCalculator()
                cal.fit(baseline, "rnaseq")
                processed = cal.transform(total_tags=bgc.total_reads)
            elif dtype == "microarray":
                baseline = load_custom_array(prebuilt, genome_assembly)
                cal = ExpTagCalculator()
                cal.fit(baseline, "array")
                processed = cal.transform(total_tags=bgc.total_reads, frag_avg_size=fragsize)
        else:
            baseline = load_rnaseq().data[prebuilt[5:]]
            cal = ExpTagCalculator()
            cal.fit(baseline, "rnaseq")
            processed = cal.transform(total_tags=bgc.total_reads)

        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()
        random_ip = honest(processed, lower=lower, upper=upper, read_type=readtype,
                           read_length=readlength, n_iter=n_iter, n_jobs=None, frag_avg_size=fragsize)
        self.progress += 25
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        # 3. peak filtering
        report = fdr_report(random_ip, called_peaks)
        if whattoshow["summary plot"]:
            rv = ReportVisualizer(report)
            rv.draw(significance_level=self.alpha)
            plt.savefig(os.path.join(self.result_folder, "peak_overview.png"))
            plt.clf()
        report_filtered = report.filter_out(self.alpha)

        #called_peaks.to_csv("cp.txt", sep="\t", index=False)
        #random_ip.to_csv("rip.txt", sep="\t", index=False)
        #report.to_csv("rep.txt", sep="\t", index=False)
        bedgraph.to_csv("smooth.bedgraph", sep="\t", index=False)
        report_filtered.to_csv("rep-f.txt", sep="\t", index=False)

        self.progress += 5
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()

        # 4. window size estimation
        cv = ClusterVisualizer(bedgraph, report_filtered)
        cv.plot_footprint(significance_level=window_level)
        self.progress += 10
        self.progressbar.setValue(self.progress)
        QtWidgets.QApplication.processEvents()  ##################################
        if whattoshow["cluster plot"]:
            plt.savefig(os.path.join(self.result_folder, "CLIP-read_clusters.png"))
            plt.clf()
        cv.windowed_peak.to_csv(os.path.join(self.result_folder, "{}%_windowed_peaks.bed".format(self.window_level_percent)), sep="\t", header=None, index=False)
        self.progress += 22
        self.progressbar.setValue(self.progress)
        plt.clf()
        QtWidgets.QApplication.processEvents()  ##################################

        if whattoshow["peak plot"]:
            filtered_peaks = ReportedPeak(report_filtered.copy())
            bv = BedGraphVisualizer(bedgraph, filtered_peaks)
            bv.draw(peak=True)
            self.progress += 3
            self.progressbar.setValue(self.progress)
            QtWidgets.QApplication.processEvents()  ##################################
            plt.savefig(os.path.join(self.result_folder, "called_peaks.png"))
            plt.clf()

        clipick_descr = self.print_summary(clip_bed_name, prebuilt, genome_assembly,
                                           smoothing, dropdup, insert,
                                           lower, upper, readtype, readlength, n_iter, self.alpha,
                             self.window_level_percent, report, cv)
        with open(os.path.join(self.result_folder, "CLIPick_summary.txt"), "w") as w:
            w.write(clipick_descr)

        self.progressbar.setValue(100)
        self.running_button.hide()
        self.complete_button.show()


    def print_summary(self, clip_bed, exp_bed, refgene,
                      smoothing, dropdup, insert,
                      lower, upper, rtype, rlen, niter, alpha, winsig,
                      report, cv):
        summary = """
############# CLIPick SUMMARY  ###############
#~~~~~~~~~~~~ Input Files Used ~~~~~~~~~~~~~~~
#  - CLIP-seq BED:  {}
#  - baseline expression: {}
#  - RefSeq genes:  {}
#
#~~~~~~~~ Peak Calling options ~~~~~~~~~~~~~~~
#  - smoothing: {}
#  - drop PCR dups: {}
#  - insert size: {} ntd
#
#~~~~~~~ in silico Random IP options ~~~~~~~~~
#~ purified insert size
#  - lower bound: {} / upper bound: {} ntd
#~ sequencing method
#  - read type:   {} read
#  - read length: {} ntd
#~ in silico Random IP
#  - number of iteration:  {}
#  - alpha (1-significance_level): {}
#  - window size determination:  {}%
#
#~~~~~~~ Summary of Significant Peaks ~~~~~~~~~
#  - Total Peaks: {}
#  - Filtered Peaks: {}
###############################################
""".format(clip_bed, exp_bed, refgene,
           smoothing, dropdup, insert,
           lower, upper, rtype, rlen,
           niter, alpha, winsig,
           report.shape[0], cv.windowed_peak.shape[0])
        #|- Description of Height of Filtered Peaks:
        #for idx, row in cv.windowed_peak.describe().iterrows():
        #    if idx == "count":
        #        summary += "|   {}:  {}\n".format(idx, int(row["score"]))
        #    elif idx == "mean":
        #        summary += "|   {}:   {}\n".format(idx, row["score"])
        #    else:
        #        summary += "|   {}:    {}\n".format(idx, row["score"])
        return summary

    def show_progress(self):
        while self.progress < 100:
            self.progress += 0.02
            self.progressbar.setValue(self.progress)
            QtWidgets.QApplication.processEvents()
        else:
            self.running_button.hide()
            self.complete_button.show()

    def show_dialog_clip_bed(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'select bed file',
                                   '~/Desktop', "(*.txt *.tsv *.csv *.bed *interval)")
        if fname[0]:
            try:
                fname = fname[0][:]
                if fname.endswith(".bed") or fname.endswith(".interval"):
                    with open(fname, "r") as r:
                        pass
                    self.path_shower.setText(fname)
                else:
                    self.path_shower.setText("error: not a .bed file")
            except TypeError:
                self.path_shower.setText("error: not a proper file")
            except FileNotFoundError:
                self.path_shower.setText("error: file not found or permission error")

    def show_dialog_exp_interval(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'select expression profile',
                                            '~/Desktop', "(*.txt *.tsv *.csv *.bed *interval)")
        try:
            fname = fname[0]
            self.exp_path_shower.setText(fname)
        except FileNotFoundError:
            self.exp_path_shower.setText("error: file not found or permission error")

    def show_dialog_output(self):
        fname = str(QtWidgets.QFileDialog.getExistingDirectory(self, 'select output directory'))
        self.result_folder = fname

    def show_dialog_refgene(self):
        if str(self.refgene_box.currentText()) == "others..":
            fname = QtWidgets.QFileDialog.getOpenFileName(self, 'select RefSeq BED',
                                        '~/Desktop', "(*.txt *.tsv *.csv *.bed *interval)")
            self.user_genome = load_custom_exome(fname[0])

    def quit_app(self):
        QtCore.QCoreApplication.exit()

    def open_finder_result(self):
        subprocess.call(["open", "-R", os.path.join(self.result_folder, "{}%_windowed_peaks.bed".format(self.window_level_percent))])

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("CLIPick", "CLIPick"))
        self.startbutton.setText(_translate("MainWindow", "START"))
        self.title.setText(_translate("MainWindow", "CLIPick"))
        self.logo_pickcheck.setText(_translate("MainWindow", "v"))
        self.subtitle.setText(_translate("MainWindow", "Analyze your CLIP-seq data"))
        self.guide1.setText(_translate("MainWindow", "1. import your CLIP-seq BED file"))
        self.guide2.setText(_translate("MainWindow", "2. import baseline expression data"))
        self.guide3.setText(_translate("MainWindow", "3. enter CLIP conditions"))
        self.horizontal_bar_start.setText(_translate("MainWindow", "——————————————————"))
        self.guide_steps.setText(_translate("MainWindow", "Four simple steps:"))
        self.guide4.setText(_translate("MainWindow", "4. choose information to be reported"))

        # --- 1st step --- #
        self.continue_button_1st.setText(_translate("MainWindow", "CONTINUE"))
        self.guide_1st_top.setText(_translate("MainWindow", "1. import your CLIP-seq BED file"))
        self.bed_label.setText(_translate("MainWindow", "CLIP-seq BED File:"))
        self.refgene_label.setText(_translate("MainWindow", "RefSeq annotation:"))
        self.dropdup_label.setText(_translate("MainWindow", "Drop PCR duplicates:"))
        self.smoothing_label.setText(_translate("MainWindow", "Peak call w/ smoothing:"))
        self.insert_label.setText(_translate("MainWindow", "Stretch tag lengths to:"))
        self.open_bed_button.setText(_translate("MainWindow", "find BED"))
        self.refgene_box.setCurrentText(_translate("MainWindow", "hg19"))
        self.refgene_box.setItemText(0, _translate("MainWindow", "hg19"))
        self.refgene_box.setItemText(1, _translate("MainWindow", "hg38"))
        self.refgene_box.setItemText(2, _translate("MainWindow", "mm8"))
        self.refgene_box.setItemText(3, _translate("MainWindow", "mm9"))
        self.refgene_box.setItemText(4, _translate("MainWindow", "mm10"))
	self.refgene_box.setItemText(5, _translate("MainWindow", "dm6"))
        self.refgene_box.setItemText(6, _translate("MainWindow", "others.."))
        self.dropdup_box.setCurrentText(_translate("MainWindow", "by start & end positions"))
        self.dropdup_box.setItemText(0, _translate("MainWindow", "by start & end positions"))
        self.dropdup_box.setItemText(1, _translate("MainWindow", "by start position only"))
        self.smoothing_box.setCurrentText(_translate("MainWindow", "Weak"))
        self.smoothing_box.setItemText(0, _translate("MainWindow", "Weak"))
        self.smoothing_box.setItemText(1, _translate("MainWindow", "Don't"))
        self.smoothing_box.setItemText(2, _translate("MainWindow", "Moderate"))
        self.smoothing_box.setItemText(3, _translate("MainWindow", "Strong"))
        self.insert_box.setCurrentText(_translate("MainWindow", "50"))
        self.insert_box.setItemText(0, _translate("MainWindow", "50"))
        self.insert_box.setItemText(1, _translate("MainWindow", "Don't"))
        self.insert_box.setItemText(2, _translate("MainWindow", "(integer)"))
        #self.exononly_box.setCurrentText(_translate("MainWindow", "mRNA"))
        #self.exononly_box.setItemText(0, _translate("MainWindow", "mRNA"))
        #self.exononly_box.setItemText(1, _translate("MainWindow", "pre-mRNA"))

        # --- 2nd step --- #
        self.continue_button_2nd.setText(_translate("MainWindow", "CONTINUE"))
        self.guide_2nd_top.setText(_translate("MainWindow", "2. import baseline expression profile"))
        self.expression_label.setText(_translate("MainWindow", "expression interval File:"))
        self.open_expression_button.setText(_translate("MainWindow", "find file"))
        #self.index_label.setText(_translate("MainWindow", "column index:"))
        self.dtype_label.setText(_translate("MainWindow", "type of data:"))
        self.dtype_box.setCurrentText(_translate("MainWindow", "RNA-Seq"))
        self.dtype_box.setItemText(0, _translate("MainWindow", "RNA-Seq"))
        self.dtype_box.setItemText(1, _translate("MainWindow", "microarray"))
        self.num_reads_label.setText(_translate("MainWindow", "OR / select data from RNA-Seq Atlas:"))
        self.num_reads_box.setCurrentText(_translate("MainWindow", "No: Use user's data"))
        self.num_reads_box.setItemText(0, _translate("MainWindow", "No: Use user's data"))
        self.num_reads_box.setItemText(1, _translate("MainWindow", "hsap_spleen"))
        self.num_reads_box.setItemText(2, _translate("MainWindow", "hsap_adipose"))
        self.num_reads_box.setItemText(3, _translate("MainWindow", "hsap_skeletalmuscle"))
        self.num_reads_box.setItemText(4, _translate("MainWindow", "hsap_hypothalamus"))
        self.num_reads_box.setItemText(5, _translate("MainWindow", "hsap_colon"))
        self.num_reads_box.setItemText(6, _translate("MainWindow", "hsap_liver"))
        self.num_reads_box.setItemText(7, _translate("MainWindow", "hsap_lung"))
        self.num_reads_box.setItemText(8, _translate("MainWindow", "hsap_ovary"))
        self.num_reads_box.setItemText(9, _translate("MainWindow", "hsap_kidney"))
        self.num_reads_box.setItemText(10, _translate("MainWindow", "hsap_testes"))
        self.num_reads_box.setItemText(11, _translate("MainWindow", "hsap_heart"))

        # --- 3rd step --- #
        self.continue_button_3rd.setText(_translate("MainWindow", "CONTINUE"))
        self.guide_3rd_top.setText(_translate("MainWindow", "3. enter CLIP conditions for random IP"))
        self.puri_label.setText(_translate("MainWindow", "purified insert size -"))
        self.fragsize_label.setText(_translate("MainWindow", "average fragment size:             nt "))
        self.sequencing_label.setText(_translate("MainWindow", "sequencing method -"))
        self.isrip_label.setText(_translate("MainWindow", "in silico Random IP -"))
        self.num_iter_label.setText(_translate("MainWindow", "number of iteration:"))
        self.lower_label.setText(_translate("MainWindow", "lower:"))
        self.upper_label.setText(_translate("MainWindow", "upper:"))
        self.readtype_label.setText(_translate("MainWindow", "read type:"))
        self.readlength_label.setText(_translate("MainWindow", "read length:"))
        self.readtype_box.setCurrentText(_translate("MainWindow", "paired-end"))
        self.readtype_box.setItemText(0, _translate("MainWindow", "paired-end"))
        self.readtype_box.setItemText(1, _translate("MainWindow", "single-end"))
        self.ntd_label.setText(_translate("MainWindow", "ntd"))
        self.ntd_label_2.setText(_translate("MainWindow", "bp"))
        self.ntd_label_3.setText(_translate("MainWindow", "bp"))
        self.num_iter_box.setCurrentText(_translate("MainWindow", "1000"))
        self.num_iter_box.setItemText(0, _translate("MainWindow", "1000"))
        self.readlength_box.setCurrentText(_translate("MainWindow", "35"))
        self.readlength_box.setItemText(0, _translate("MainWindow", "35"))
        self.fragsize_box.setCurrentText(_translate("MainWindow", "50"))
        self.fragsize_box.setItemText(0, _translate("MainWindow", "50"))
        self.lower_box.setCurrentText(_translate("MainWindow", "20"))
        self.lower_box.setItemText(0, _translate("MainWindow", "20"))
        self.upper_box.setCurrentText(_translate("MainWindow", "80"))
        self.upper_box.setItemText(0, _translate("MainWindow", "80"))

        # --- 4th step --- #
        self.run_button.setText(_translate("MainWindow", "RUN CLIPick"))
        self.guide_4th_top.setText(_translate("MainWindow", "4. choose information to be reported"))
        self.window_label.setText(_translate("MainWindow", "peaks with            % window"))
        self.alpha_label.setText(_translate("MainWindow", "FDR cutoff: "))
        self.window_box.setItemText(0, _translate("MainWindow", "95"))
        self.window_box.setItemText(1, _translate("MainWindow", "99"))
        self.window_box.setItemText(2, _translate("MainWindow", "90"))
        self.alpha_box.setItemText(0, _translate("MainWindow", "0.05"))
        self.alpha_box.setItemText(1, _translate("MainWindow", "0.01"))
        self.alpha_box.setItemText(2, _translate("MainWindow", "0.1"))
        self.signi_peak_label.setText(_translate("MainWindow", "significant peaks"))
        self.cluster_label.setText(_translate("MainWindow", "cluster analysis"))
        self.peak_overview_label.setText(_translate("MainWindow", "FDR overview"))
        self.clipick_summary_label.setText(_translate("MainWindow", "CLIPick summary"))
        self.peak_call_label.setText(_translate("MainWindow", "selected peaks"))
        self.signi_peak_sup_label.setText(_translate("MainWindow", "Final output text"))
        self.figures_label.setText(_translate("MainWindow", "Figures"))
        self.processing_label.setText(_translate("MainWindow", "click 'Run CLIPick' to select directory to save outputs"))
        self.processing_label2.setText(_translate("MainWindow", "processing..."))
        self.running_button.setText(_translate("MainWindow", "RUNNING..."))
        self.complete_button.setText(_translate("MainWindow", "COMPLETE"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
