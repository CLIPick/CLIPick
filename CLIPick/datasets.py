# COPYRIGHT (C) 2016 By Park, Sihyung
# * Copyight of each sample datasets belongs to the organization provided the data.
# module for loading sample or custom data - RNA-seq, microarray, CLIP-seq, etc.

"""
module for loading sample datasets.
Sample datasets include:
  1. RNA-Seq Atlas (rev1)
  2. Heart AGO2-binding sites (GSE83410)
"""
import sys, os
import numpy as np
import pandas as pd
import sqlite3 as db
from .__init__ import get_data, get_refseq_data, get_exome_data
from .utils import RNASeq, MicroArray, IPTags

class load_rnaseq(object):
    """
    Loads baseline expression profile of human tissues.
    Original data were retrieved from RNA Seq Atlas.
    : http://medicalgenomics.org/rna_seq_atlas/download

    * Usage
    >>> rnaseq = load_rnaseq()
    >>> rnaseq.data.keys()
    ['heart', 'testes', 'spleen', 'adipose', 'skeletalmuscle',
    'hypothalamus', 'colon', 'liver', 'lung', 'ovary', 'kidney']
    >>> type(rnaseq.data["heart"])
    <class 'pandas.core.frame.DataFrame'>
    >>> rnaseq.data["heart"].head()
          RefSeq  length  expression
    0  NM_178450    4135       1.429
    1  NM_020814    4450       0.054
    2  NM_017824    3926       3.153
    3  NM_019106    4656       0.092
    4  NM_004574    1736       4.983

    """
    def __init__(self):
        file_path = get_data("RNA_Seq_Atlas_rev1(raw).txt")
        self.data = parse_rsatlas(file_path)
        self.DESCR = """
    ---------------- RNA Seq Atlas (rev1) - human tissues ----------------
        The provided genome-wide expression compendium originates from
      eleven, healthy, human tissues samples pooled from multiple donors
      spanning 32384 specific transcripts corresponding to 21399 unique
      genes. The tissues include adipose, colon, heart, hypothalamus,
      kidney, liver, lung, ovary, skeletal muscle, spleen and testes
      (for more information see: Castle et al. 2010).
    """


class load_p13array(object):
    """
    """
    def __init__(self, assembly="mm8"):
        # p13_mouse_affy_averaged.txt
        file_path = get_data("p13_mouse_affy_averaged.txt")
        self.data = MicroArray(load_custom_array(file_path, assembly))
        self.DESCR = """
    ---------------- P13 mouse neocortex microarray data ----------------
    - Retrieved from Gene Expression Omnibus: GSE16338
    - Averaged signal values of three samples
    * Status: Public on May 30, 2009
    * Title: P13 cortex RNA
    * Organism: Mus musculus
    * Experiment type: Expression profiling by array
    * Summary
      : To estimate transcripts levels in P13 mouse neocortex. MicroRNAs
       (miRNAs) play critical roles in the regulation of gene expression.
       Recently, high-throughput sequencing of RNAs isolated by crosslinking
       immunoprecipitation (HITS-CLIP) has identified functional protein-RNA
       interaction sites. We used HITS-CLIP to covalently crosslink native
       Argonaute (Ago) protein-RNA complexes in mouse brain. This produced
       two simultaneous datasets-Ago-miRNA and Ago-mRNA binding sites-that
       were combined with bioinformatic analysis to identify miRNA-target
       mRNA interaction sites. We validated genome-wide interaction maps for
       miR-124, and generated additional maps for the 20 most abundant
       miRNAs present in P13 mouse brain. Here we include the expression
       data obtained from dissected P13 mouse neocortex. These data are used
       for in silico CLIP simulation for normalization of Ago HITS-CLIP tags
       and also for selecting transcripts expressed in P13 mouse cortex.
    * Overall design
      : RNA from the neocortex of 3 wild type P13 CD1 mice. One array per
       biological replicate.
    * Contributor(s): Chi SW, Darnell RB
    * Citation(s)
      : Chi SW, Zang JB, Mele A, Darnell RB. Argonaute HITS-CLIP decodes
       microRNA-mRNA interaction maps. Nature 2009 Jul 23;460(7254):479-86.
       PMID: 19536157
    * Submission date: May 28, 2009
    * Last update date: Nov 14, 2016
    * Contact name: Sung Wook Chi
    * E-mail: schi@rockefeller.edu
    * Organization name: Rockefeller Univ
    * Street address: 1230 York Ave,
    * City: New York
    * State/province: NY
    * ZIP/Postal code: 10065
    * Country: USA
    """


class load_p13clip(object):
    """
    Returns AGO HITS-CLIP tags BED file path from P13 mouse neocortex.

    * Usage
    >>> clip = load_p13clip()
    >>> clip.data
    """
    def __init__(self):
        file_path = get_data("Tags_OL_7G1-1_2A8_RefSeq.bed")
        origin = pd.read_csv(file_path, sep='\t', header=None)
        self.data = origin
        self.DESCR = """
    ------------- AGO HITS-CLIP data from p13 mouse neocortex -------------
    """


class load_ago2clip(object):
    """
    Loads AGO2 CLIP data from human heart.
    Original data were retrieved from GEO (GSE83410).
    : https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE83410

    * Usage
    >>> clip = load_ago2clip()
    >>> clip.data.head()
          0          1          2                                              3  \
    0  chr1   25166476   25166527     PEAK575|chr1:+:25166476:25166527|1.835E-31
    1  chr1   25168280   25168330     PEAK329|chr1:+:25168280:25168330|4.680E-08
    2  chr1   25168171   25168222    PEAK1058|chr1:+:25168171:25168222|2.159E-06
    3  chr1  175937535  175937595  PEAK2635|chr1:-:175937535:175937595|3.418E-47
    4  chr1  175937530  175937581   PEAK251|chr1:-:175937530:175937581|2.892E-04

               4  5   6             7  \
    0   70.77310  +  51     NM_013943
    1   16.87740  +  50     NM_013943
    2   13.04590  +  51     NM_013943
    3  106.99200  -  60  NM_001286644
    4    8.14839  -  51  NM_001286644

                                                       8
    0  GCAATGAAATGACATTAGCTGATTGCAACCTGCTGCCCAAACTGCA...
    1  AATCATAGAGCTTGCTATTTGTACATCTGTTGAGCAACACTACATA...
    2  GATGGGCACCTGATACTCTGTCTAAATACGTTTGTTATATGTGTTT...
    3  TATGGAGGTCTCTGTCTGGCTTAGGACAGCTGGCTAAGTCTGATCG...
    4  TCTGGCTTAGGACAGCTGGCTAAGTCTGATCGTTCCCCTCCGTACA...
    >>> clip.DESCR

    """
    def __init__(self):
        file_path = get_data("CLIP_sample(GSE83410).interval")
        origin = pd.read_csv(file_path, sep='\t', header=None)
        self.data = IPTags(pd.concat([origin.ix[:, i] for i in [7, 3, 4]], axis=1))
        self.data.columns = ["RefSeq", "peak name", "peak_height"]
        self.DESCR = """
    ---------------- AGO2 CLIP data from human heart ----------------
    - Retrieved from Gene Expression Omnibus: GSE83410 / GSM2202476
    * Status: Public on Aug 09, 2016
    * Title: Elucidation of Transcriptome-Wide MicroRNA Binding Sites
             in Human Cardiac Tissues by Ago2 HITS-CLIP
    * Organism: Homo sapiens
    * Experiment type
      : Expression profiling by high throughput sequencing
    * Summary
      : We deciphered Ago2:RNA interactions in post-mortem human
       heart tissues using crosslinking immunoprecipitation coupled
       with high-throughput sequencing (HITS-CLIP) to generate the
       first transcriptome-wide map of miR targeting events in human
       myocardium, detecting 4000 cardiac Ago2 binding sites across
       >2200 target transcripts.
    * Overall design
      : Profiling AGO2-associated RNAs in cardiac tissues obtained
       from 6 donors with heart diseases of differing etiologies.
    * Contributor(s): Boureau RL, Spengler RM
    * Citation(s)
      : Spengler RM, Zhang X, Cheng C, McLendon JM et al. Elucidation
       of transcriptome-wide microRNA binding sites in human cardiac
       tissues by Ago2 HITS-CLIP.
       Nucleic Acids Res 2016 Sep 6;44(15):7120-31. PMID: 27418678
    * Submission date: Jun 15, 2016
    * Last update date: Sep 27, 2016
    * Contact name: Ryan L Boudreau
    * E-mail: boudreau.ryan@gmail.com
    * Organization name: University of Iowa
    * Department: Internal Medicine
    * Lab: 200 EMRB
    * Street address: 500 Newton Rd
    * City: Iowa City
    * State/province: IA
    * ZIP/Postal code: 52246
    * Country: USA
    """


class load_genome(object):
    """
    Loads annotated genome RefSeq retrieved from UCSC genome browser.
    loadable genomes: human(hg16~hg38), ...

    * Usage
    >>> genome = load_genome("hg19")
    >>> type(genome.data)
    <class 'pandas.core.frame.DataFrame'>
    >>> genome.data.head()
    """
    def __init__(self, genome_assembly):
        #file_path = get_refseq_data("UCSC_{}.bed".format(genome_assembly.lower()))
        con = db.connect(get_refseq_data())
        try:
            self.data = pd.read_sql("select * from {}".format(genome_assembly), con)
        except IOError:     # pragma: no cover
            raise IOError("invalid genome: {}".format(genome_assembly))
        self.DESCR = """
        ---------------------- Genome Assembly from UCSC ----------------------
        """


class load_exome(object):
    """
    Loads annotated genome RefSeq retrieved from UCSC genome browser.
    loadable genomes: human(hg16~hg38), ...

    * Usage
    >>> exome = load_exome("hg19")
    >>> type(exome.data)
    <class 'pandas.core.frame.DataFrame'>
    >>> exome.data.head()
    """
    def __init__(self, genome_assembly):
        #file_path = get_refseq_data("UCSC_{}.bed".format(genome_assembly.lower()))
        con = db.connect(get_exome_data())
        try:
            self.data = pd.read_sql("select * from {}".format(genome_assembly), con)
        except IOError:     # pragma: no cover
            raise IOError("invalid genome: {}".format(genome_assembly))
        self.DESCR = """
        ---------------------- Genome Assembly from UCSC ----------------------
        """


def load_custom_genome(file_path):
    genome = pd.read_csv(file_path, header=None, sep="\t",
              names=["chrom", "chrstart", "chrend", "name", "score", "strand",
                       "thickstart", "thickend", "itemRGB", "blockcount",
                       "blocksizes", "blockstarts"])
    return genome


def load_custom_exome(file_path):
    """
    Loads annotated genome RefSeq retrieved from Table Browser of UCSC genome browser.
    Genome must be retrieved as BED format, with each line contains one RefSeq exon info.

    * Usage
    >>> exome = load_custom_exome("/path/to/your/exome/file")
    >>> type(exome)
    <class 'pandas.core.frame.DataFrame'>
    >>> exome.head()
    """
    exome = pd.read_csv(file_path, header=None, sep="\t",
              names=["chrom", "chrstart", "chrend", "name", "score", "strand"])
    return exome


def load_custom_rnaseq(file_path, genome_assembly, header=None):
    """
    Loads custom(yours!) RNA-Seq data file.

    * Input
     - file_path: name of custom file to read.
       File should be in tab-seperated-text-like format.
       (Galaxy interval format, BED format, etc.)
     - genome_assembly: a string, e.g., "hg19", or a DataFrame object of custom
       RefSeq loaded with `load_custom_genome()`.
    """
    origin = pd.read_csv(file_path, sep="\t", header=header)
    origin.columns = ["RefSeq", "expression"]
    res = match_to_refseq(origin, genome_assembly)
    res.drop_duplicates(["RefSeq"], keep="first", inplace=True)
    return RNASeq(res)


def load_custom_array(file_path, genome_assembly, header=None):
    """
    Loads custom(yours!) microarray data file.

    * Input
     - file_path: path of custom file to read.
     - genome_assembly: a string, e.g., "hg19", or a DataFrame object of custom
       RefSeq loaded with `load_custom_genome()`.
    """
    origin = pd.read_csv(file_path, sep="\t", header=header)
    origin.columns = ["RefSeq", "expression"]
    res = match_to_refseq(origin, genome_assembly)
    res.drop_duplicates(["RefSeq"], keep="first", inplace=True)
    return MicroArray(res)


def load_custom_ip(file_path, col_index, header=None):
    """
    Loads custom(yours!) called peaks data from IP-seq experiment.

    * Input
     - file_path: name of custom file to read.
     - col_index: a list of length 3.
         i.e. when col_index=[0, 3, 2], each number in a list represents indices
         of columns of custom file corresponding to RefSeq name, peak name,
         peak height.
    """
    if len(col_index) != 3:     # pragma: no cover
        raise ValueError("col_index should be a list of length 3")

    origin = pd.read_csv(file_path, sep="\t", header=header)
    res = pd.concat([origin.iloc[:, i] for i in col_index], axis=1)
    res.columns = ["RefSeq", "peak name", "peak_height"]
    return IPTags(res)


def parse_rsatlas(rna_seq_atlas_in):
    rna_seq_atlas = open(rna_seq_atlas_in, "r")
    r = rna_seq_atlas.readline()
    tissues = r.rstrip().split("\t")
    # adipose	colon	heart	hypothalamus	kidney	liver
    # lung	ovary	skeletalmuscle	spleen	testes

    rsatlas = {}
    for i in range(5, 16):
        rsatlas[tissues[i]] = []

    r = rna_seq_atlas.readline()
    l = r.rstrip().split("\t")
    while r:
        for i in range(5, 16):
            rsatlas[tissues[i]].append([l[3], int(l[4]), float(l[i])])
        r = rna_seq_atlas.readline()
        l = r.rstrip().split("\t")
    for i in range(5, 16):
        rsatlas[tissues[i]] = RNASeq(pd.DataFrame(rsatlas[tissues[i]],
            columns=["RefSeq", "length", "expression"]))
    rna_seq_atlas.close()
    return rsatlas
# rsatlas = parse_rsatlas("./sample_data/RNA_Seq_Atlas_rev1(raw).txt")
# rsatlas  ~  {tissue: [[transcript_ID, transcript_len, expression], ...]}
#               str           str            int       float


def match_to_refseq(pre_matched_profile, genome_assembly):
    # ["chrom", "chrstart", "chrend", "peak", "peak_height", "peak_pos", "strand"]

    if isinstance(genome_assembly, str):       # pragma: no cover
        con = db.connect(get_refseq_data())
        genome_assembly = pd.read_sql("SELECT * FROM {}".format(genome_assembly), con)
        con.close(); del con

    joined = pre_matched_profile.merge(genome_assembly, left_on="RefSeq", right_on="name")
    joined["length"] = joined["mRNA_length"]   #= joined.blocksizes.str.slice(0, -1).str.split(",").apply(lambda x: sum(map(int, x)))
    res = pd.concat([joined["RefSeq"], joined["length"], joined["expression"]], axis=1)

    return res
