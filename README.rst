CLIPick: Package for analysis of HITS-CLIP and its variations
=============================================================

**CLIPick** is a Package for bioinformatic analysis of high-throughput sequencing coupled with cross-linking followed by immunoprecipitation (HITS-CLIP or CLIP-seq) reads.

Copyright (c) 2016 by Sihyung Park, Sung Wook Chi.

- Docs can be found here: https://naturale0.github.io/CLIPick/


Key features
=============
1. HITS-CLIP Peak calling
2. *in silico* Random IP: peak significancy test
3. RNAbp footprint analysis: window size determination
4. Visualization: `samples <https://naturale0.github.io/CLIPick/graphics/#gallery>`_


Dependency
==========

`CLIPick` heavily depends on the packages listed below:
 - `NumPy <http://www.numpy.org>`_ >= 1.11.1
 - `SciPy <http://scipy.org>`_ >= 0.18.1
 - `Pandas <http://pandas.pydata.org>`_ >= 0.18.1
 - `matplotlib <http://matplotlib.org>`_ >= 1.5.1
 - `pybedtools <http://pypi.python.org/pypi/pybedtools>`_ >= 0.7.8

   - pybedtools requires `BedTools <http://bedtools.readthedocs.io/en/latest/>`_

 - `Cython <http://cython.org>`_ >= 0.25.1 (optional)


