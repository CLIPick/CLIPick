from setuptools import setup, find_packages, Extension
from codecs import open
import os

try:
    from Cython.Distutils import build_ext
except ImportError:
    use_cython = False
else:
    use_cython = True

cmdclass = { }
ext_modules = [ ]

cwd = os.path.abspath(os.path.dirname(__file__))
reqs = open(os.path.join(cwd, "requirements.txt"), encoding="utf-8").read().split("\n")

install_requires = [x.strip() for x in reqs if "git+" not in x]
dependency_links = [x.strip().replace("git+", "") for x in reqs if x.startswith("git+")]

if use_cython:
    ext_modules += [Extension("CLIPick.utils", ["CLIPick/utils.pyx"])]
    cmdclass.update({"build_ext": build_ext })
else:
    ext_modules += [Extension("CLIPick.utils", ["CLIPick/utils.c"])]

setup(
    name="CLIPick",
    version="0.1.0b1",
    author="Sihyung Park, Sung Wook Chi",
    author_email="taran_sh@korea.ac.kr",
    description="Package for bioinformatic analysis of high-throughput sequencing coupled with cross-linking followed by immunoprecipitation (HITS-CLIP or CLIP-seq) reads",
    url="https://github.com/Sihyung-Park/CLIPick",
    packages=find_packages(),
    package_data={"CLIPick": ["sample_data/*", "refseq_data/*"]},
    cmdclass=cmdclass,
    ext_modules=ext_modules,
    #license=,
    long_description=open("README.rst", encoding="utf-8").read(),
    install_requires=install_requires,
    dependency_links=dependency_links,
)
