# CLIPick-GUI: GUI implementation of CLIPick module

![GUI_main_image](./docs/CLIPick_GUI.png)

**CLIPick** is a Package for bioinformatic analysis of CLIP-seq reads.

*CLIPick-GUI runs only with Python 3.*

Copyright (c) 2016 by Sihyung Park, Sung Wook Chi.

- Docs can be found [here](https://naturale0.github.io/CLIPick/).
- Please star the project [[GUI](https://gitlab.com/chi_lab/CLIPick), [package](https://gitlab.com/chi_lab/CLIPick-package)] if you like it.


&ensp;

# Key features

1. HITS-CLIP Peak calling
2. *in silico* Random IP: peak significancy test
3. RNAbp footprint analysis: window size determination
4. Visualization: [samples](https://naturale0.github.io/CLIPick/graphics/#gallery)


&ensp;

# Quick start (CLIPick-GUI for macOS)

In UNIX terminal, follow commands below (example based on macOS 10.12.1).

```
$ git clone https://github.com/naturale0/CLIPick
$ git checkout CLIPick_GUI
$ cd CLIPick
```

Run python script by

```
$ python3 setup.py install
$ cd CLIPick
$ python3 CLIPick_GUI.py
```

You need to have [NumPy](http://www.numpy.org), [SciPy](http://scipy.org), [pandas](http://pandas.pydata.org), [matplotlib](http://matplotlib.org), [BedTools](http://bedtools.readthedocs.io/en/latest/), and [PyQt5](https://www.riverbankcomputing.com/software/pyqt/) installed on your computer for `CLIPick_GUI` to work properly.

**You need to install BedTools manually!** Please download and install BedTools following [the guides](http://bedtools.readthedocs.io/en/latest/content/installation.html).


&ensp;

# Documentation

For full docs, please visit https://naturale0.github.io/CLIPick/

&ensp;

# Dependency

`CLIPick` heavily depends on the packages listed below:

* Python packages
 - [NumPy](http://www.numpy.org) >= 1.11.1
 - [SciPy](http://scipy.org) >= 0.18.1
 - [Pandas](http://pandas.pydata.org) >= 0.18.1, <= 0.22.0
 - [matplotlib](http://matplotlib.org) >= 1.5.1, <= 2.2.2
 - [PyQt5](https://www.riverbankcomputing.com/software/pyqt/) >= 5.7
 - [Cython](http://cython.org) >= 0.25.1 (optional)
* others (**NEED MANUAL INSTALL**)
 - [BedTools](http://bedtools.readthedocs.io/en/latest/) >= 2.25.0

&ensp;



# Cite Us!

```
Park S., Ahn S. H., Cho E. S., Cho Y. K., Jang E., Chi S. W., CLIPick: a sensitive peak caller for expression-based deconvolution of HITS-CLIP signals, Nucleic Acids Research, Volume 46, Issue 21, 30 November 2018, Pages 11153–11168, https://doi.org/10.1093/nar/gky917
```

For BibTex,

```
@article{10.1093/nar/gky917,
    author = {Park, Sihyung and Ahn, Seung Hyun and Cho, Eun Sol and Cho, You Kyung and Jang, Eun-Sook and Chi, Sung Wook},
    title = "{CLIPick: a sensitive peak caller for expression-based deconvolution of HITS-CLIP signals}",
    journal = {Nucleic Acids Research},
    volume = {46},
    number = {21},
    pages = {11153-11168},
    year = {2018},
    month = {10},
    issn = {0305-1048},
    doi = {10.1093/nar/gky917},
    url = {https://doi.org/10.1093/nar/gky917},
    eprint = {https://academic.oup.com/nar/article-pdf/46/21/11153/26901551/gky917.pdf},
}
```

&ensp;

# References
- Random IP - modified and enhanced from *in silico* Random CLIP:  
Chi, S. W., Zang, J. B., Mele, A., & Darnell, R. B. (2009). Argonaute HITS-CLIP decodes microRNA–mRNA interaction maps. Nature. doi:10.1038/nature08170
- bedtools 2 (for BED-to-BEDGraph conversion only):  
Quinlan AR and Hall IM, 2010. BEDTools: a flexible suite of utilities for comparing genomic features. Bioinformatics. 26, 6, pp. 841–842.
- RNA-Seq Atlas:  
RNA-Seq Atlas - A reference database for gene expression profiling in normal tissue by next generation sequencing. Bioinformatics (Oxford, England), 10.1093/bioinformatics/bts084 (Krupp et al.).
- P13 mouse neocortex exon-array:  
Chi, S. W., Zang, J. B., Mele, A., & Darnell, R. B. (2009). Argonaute HITS-CLIP decodes microRNA–mRNA interaction maps. Nature. doi:10.1038/nature08170
- human cardiac tissue AGO2 HITS-CLIP:  
Spengler RM, Zhang X, Cheng C, McLendon JM et al. Elucidation of transcriptome-wide microRNA binding sites in human cardiac tissues by Ago2 HITS-CLIP. Nucleic Acids Res 2016 Sep 6;44(15):7120-31. PMID: 27418678
- Genome reference assemblies were retrieved from [UCSC genome browser](http://genome.ucsc.edu/):  
Kent WJ, Sugnet CW, Furey TS, Roskin KM, Pringle TH, Zahler AM, Haussler D. The human genome browser at UCSC. Genome Res. 2002 Jun;12(6):996-1006.
- Human RefSeq genes (hg16, hg17, hg18, hg19, hg38):  
The Genome Sequencing Consortium. Initial sequencing and analysis of the human genome. Nature. 2001 Feb 15;409(6822):860-921.
- Mouse RefSeq genes (mm7, mm8, mm9):  
Mouse Genome Sequencing Consortium. Initial sequencing and comparative analysis of the mouse genome. Nature. 2002 Dec 5;420(6915):520-62. PMID: 12466850
- Mouse RefSeq genes (mm10) is provided by the [Genome Reference Consortium (GRC)](https://www.ncbi.nlm.nih.gov/grc)([acknowledgements](https://www.ncbi.nlm.nih.gov/grc/mouse)), and retrieved from UCSC genome browser.
