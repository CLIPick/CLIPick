numpy >= 1.11.1
scipy >= 0.18.1
Cython >= 0.25.1
pandas >= 0.18.1, <= 0.22.0
matplotlib >= 1.5.1, <= 2.2.2
PyQt5 >= 5.7
